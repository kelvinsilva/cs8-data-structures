#include <iostream>
#include "kelvinsListHelpers.h"


using namespace std;

void kelvinsMergeSort(kelvinsNode<int>* &sortThis);

kelvinsNode<int>* mergeTwoSortedLists(kelvinsNode<int>* leftNode, kelvinsNode<int>* rightNode);

int main(){
  
  kelvinsNode<int>* nodeHead = NULL;
  
  insertHead(nodeHead, 9999999);
  insertHead(nodeHead, 5);  
  insertHead(nodeHead, 15);
  insertHead(nodeHead, -22);  
  insertHead(nodeHead, 69);
  insertHead(nodeHead, -9);                    
  insertHead(nodeHead, 20);
  insertHead(nodeHead, 9999);
  
  cout << "\nunsorted List:";
  printList(nodeHead, ", ");
    
  kelvinsMergeSort(nodeHead);
  
  cout << "\nsortedList:";
  printList(nodeHead, ", ");
  cout << endl;
  
  return 0;
}

kelvinsNode<int>* mergeTwoSortedLists(kelvinsNode<int>* leftNode, kelvinsNode<int>* rightNode){
  
  if (leftNode == NULL) {
    return rightNode;
  }
  if (rightNode == NULL) {
    return leftNode;
  }
  
  kelvinsNode<int>* tempNode = mergeList(leftNode, rightNode);
  sortList(tempNode);  //sortList calls insertSorted which is an algorithm for sorting lists which may or may not be
  return tempNode;

}

void kelvinsMergeSort(kelvinsNode<int>* &sortThis){

  kelvinsNode<int>* left = NULL;
  kelvinsNode<int>* right = NULL;
  
  if (sortThis == NULL || getCount(sortThis) == 1){
    
    return;
  }else {
    
    //split in half.
    splitList(sortThis, At(sortThis, (getCount(sortThis)/2)), left, right);
    
    kelvinsMergeSort(left);
    kelvinsMergeSort(right);
    
    sortThis = mergeTwoSortedLists(left, right);
  }


}