#ifndef _KELVINS_NODE_H_
#define _KELVINS_NODE_H_

#include <iostream>

using namespace std;


template <typename TYPE>
class kelvinsNode {

private:

    kelvinsNode *nextAddress;
    kelvinsNode *previousAddress;
    TYPE value;


public:

    kelvinsNode();
    kelvinsNode(TYPE value);

    kelvinsNode* getCurrentAddress();
    kelvinsNode* getNextAddress();
    kelvinsNode* getPreviousAddress();

    TYPE getCurrentValue();
    TYPE& getCurrentValueReference();

    void setCurrentAddress(kelvinsNode* nodePointer);
    void setNextAddress(kelvinsNode* nodePointer);
    void setPreviousAddress(kelvinsNode* nodePointer);
    void setCurrentValue(TYPE val);

};

template <typename TYPE>
TYPE& kelvinsNode<TYPE>::getCurrentValueReference(){

    return value;
}


template <typename TYPE>
kelvinsNode<TYPE>::kelvinsNode(){

    nextAddress = NULL;
    previousAddress = NULL;
}

template <typename TYPE>
kelvinsNode<TYPE>::kelvinsNode(TYPE val){

    value = val;
    nextAddress = NULL;
    previousAddress = NULL;
}

template <typename TYPE>
void kelvinsNode<TYPE>::setNextAddress(kelvinsNode* nodePointer){

    nextAddress = nodePointer;
}

template <typename TYPE>
void kelvinsNode<TYPE>::setPreviousAddress(kelvinsNode* nodePointer){

    previousAddress = nodePointer;
}

template <typename TYPE>
void kelvinsNode<TYPE>::setCurrentValue(TYPE val){

    value = val;
}

template <typename TYPE>
kelvinsNode<TYPE>* kelvinsNode<TYPE>::getCurrentAddress(){

    return this;
}

template <typename TYPE>
kelvinsNode<TYPE>* kelvinsNode<TYPE>::getNextAddress(){

    return nextAddress;
}

template <typename TYPE>
kelvinsNode<TYPE>* kelvinsNode<TYPE>::getPreviousAddress(){

    return previousAddress;
}

template <typename TYPE>
TYPE kelvinsNode<TYPE>::getCurrentValue(){

    return value;
}

#endif // _KELVINS_NODE_H_
