#ifndef KELVINS_LIST_HELPERS_H_
#define KELVINS_LIST_HELPERS_H_

#include <iostream>
#include <time.h>
#include <cstdlib>

#include "kelvinsNode.h"

using namespace std;

template <typename TYPE_ONE>
class ascendingOrder{

public:
    bool operator()(TYPE_ONE left, TYPE_ONE right){
        return (left < right);
    }
};

template <typename TYPE_ONE>
class descendingOrder{

public:
    bool operator()(TYPE_ONE left, TYPE_ONE right){
        return (left > right);
    }

};

template <typename T>
class doNothing{

public:
    void operator()(T left){

    }
};

template <typename T>
class printMe{

public:
    void operator()(T& left){
        cout << left;
    }
};

template <typename T>
class addMe{

public:
    void operator()(T& left){
        ++left;
    }
};

//usage: _function specifies the ordering in which to sort, and _functionTwo applies a certain function to the value of the kelvinsNode if a collision exists.
template <typename TYPE, typename FUNC = ascendingOrder<TYPE>, typename FUNC_TWO = doNothing<TYPE> >
int insertUniqueSorted(kelvinsNode<TYPE>* &head, TYPE item, FUNC _function = ascendingOrder<TYPE>() , FUNC_TWO _functionTwo = doNothing<TYPE>() );

template <typename TYPE>
int insertHead( kelvinsNode<TYPE>* &head, TYPE item);

template<typename TYPE>
int deleteList(kelvinsNode<TYPE>* &head);

template <typename TYPE>
int insertAfter( kelvinsNode<TYPE>* head, kelvinsNode<TYPE> *afterThis, TYPE item);

template <typename TYPE>
int insertBefore(kelvinsNode<TYPE>* head, kelvinsNode<TYPE>* beforeThis, TYPE item);

template <typename TYPE, typename FUNC = ascendingOrder<TYPE> >
int insertSorted(kelvinsNode<TYPE>* &head, TYPE item, FUNC _function = ascendingOrder<TYPE>() );

template <typename TYPE>
int deleteNode( kelvinsNode<TYPE>* &head, kelvinsNode<TYPE> *deleteThis);

template <typename TYPE>
int deleteItem(kelvinsNode<TYPE>* &head, TYPE value);

template <typename TYPE>
kelvinsNode<TYPE>* searchNode(kelvinsNode<TYPE>* head, TYPE item);

template <typename TYPE>
int printList(kelvinsNode<TYPE>* head, string delimiter = ", ");

template <typename TYPE>
kelvinsNode<TYPE>* getNodeBefore(kelvinsNode<TYPE>* head, kelvinsNode<TYPE>* beforeThis);

template <typename TYPE>
kelvinsNode<TYPE>* At(kelvinsNode<TYPE>* head, int count);

template <typename TYPE>
int getCount(kelvinsNode<TYPE>* head);

template <typename TYPE>
int reverseList(kelvinsNode<TYPE>* &head);

template <typename TYPE>
int deleteRepeats(kelvinsNode<TYPE>* &head);

template <typename TYPE>
int sortList(kelvinsNode<TYPE>* &head);

template <typename TYPE>
int shuffleList(kelvinsNode<TYPE>* &head);

template <typename TYPE>
int copyList(kelvinsNode<TYPE>* head, kelvinsNode<TYPE>* &copyToThis);

template <typename TYPE>
kelvinsNode<TYPE>* mergeList(kelvinsNode<TYPE>* listOne, kelvinsNode<TYPE>* listTwo);

template <typename TYPE>
kelvinsNode<TYPE>* uniqueMergeList(kelvinsNode<TYPE>* listOne, kelvinsNode<TYPE>* listTwo);

template <typename TYPE>
int splitList(kelvinsNode<TYPE>* head, kelvinsNode<TYPE>* at, kelvinsNode<TYPE>* &left, kelvinsNode<TYPE>* &right);




template <typename TYPE, typename FUNC = ascendingOrder<TYPE>, typename FUNC_TWO = doNothing<TYPE> >
int insertUniqueSorted(kelvinsNode<TYPE>* &head, TYPE item, FUNC _function = ascendingOrder<TYPE>() , FUNC_TWO _functionTwo = doNothing<TYPE>() ){

    kelvinsNode<TYPE>* tempUnique = searchNode(head, item);


    if (tempUnique == NULL){

        insertSorted(head, item, _function);
    }else {
        _functionTwo( tempUnique->getCurrentValueReference() );   //apply custom functor.
        return 1;
    }

}

template <typename TYPE>
int splitList(kelvinsNode<TYPE>* head, kelvinsNode<TYPE>* at, kelvinsNode<TYPE>* &left, kelvinsNode<TYPE>* &right){

  if (getCount(head) == 1){
    
    insertHead(left, head->getCurrentValue());
    right = NULL;
    
  }else if (getCount(head) == 0){
    //do nothing
    
  }else{
  
    kelvinsNode<TYPE>* newList = NULL;
    kelvinsNode<TYPE>* tempTraverse = head;
    while (tempTraverse != at){

        insertHead(newList, tempTraverse->getCurrentValue() );
        tempTraverse = tempTraverse->getNextAddress();
    }
    reverseList(newList);
    left = newList;
    tempTraverse = at;
    newList = NULL;

    while(tempTraverse != NULL){

        insertHead(newList, tempTraverse->getCurrentValue());
        tempTraverse = tempTraverse->getNextAddress();
    }
    reverseList(newList);
    right = newList;
  }
  
    return 1;

}

template<typename TYPE>
int deleteList(kelvinsNode<TYPE>* &head){

    while(head != NULL){

        deleteNode(head, head);
    }
    return 1;
}

template <typename TYPE>
kelvinsNode<TYPE>* uniqueMergeList(kelvinsNode<TYPE>* listOne, kelvinsNode<TYPE>* listTwo){

    kelvinsNode<TYPE>* tempNode = mergeList(listOne, listTwo);
    deleteRepeats(tempNode);
    return tempNode;
}

template <typename TYPE>
kelvinsNode<TYPE>* mergeList(kelvinsNode<TYPE>* listOne, kelvinsNode<TYPE>* listTwo){

    kelvinsNode<TYPE>* newList = NULL;
    copyList(listOne, newList);
    int count = getCount(newList) - 1;
    kelvinsNode<TYPE>* tempPtr = At(newList, count);
    tempPtr->setNextAddress(listTwo);

    return newList;
}

template <typename TYPE>
int copyList(kelvinsNode<TYPE>* head, kelvinsNode<TYPE>* &copyToThis){

    kelvinsNode<TYPE>* tempTraverse = head;

    while(tempTraverse != NULL){

        insertHead(copyToThis, tempTraverse->getCurrentValue());
        tempTraverse = tempTraverse->getNextAddress();
    }
    reverseList(copyToThis);

    return 1;
}

template <typename TYPE>
int shuffleList(kelvinsNode<TYPE>* &head){

    srand(time(NULL));

    int randAt = rand()%getCount(head) - 1;
    kelvinsNode<TYPE>* newList = NULL;
    kelvinsNode<TYPE>* tempTraverse = head;
    kelvinsNode<TYPE>* tempInsert = At(head, randAt);
    kelvinsNode<TYPE>* tempTempInsert = tempInsert;

    insertHead(newList, tempInsert->getCurrentValue());
    tempInsert = tempInsert->getNextAddress();

    while (tempInsert != NULL){

        insertHead(newList, tempInsert->getCurrentValue());
        tempInsert = tempInsert->getNextAddress();
    }
    while(tempTraverse != tempTempInsert){

        insertHead(newList, tempTraverse->getCurrentValue());
        tempTraverse = tempTraverse->getNextAddress();
    }
    tempTraverse = head;
    while (tempTraverse != NULL){
        deleteNode(tempTraverse, tempTraverse);
    }
    head = newList;

    return 1;
}

template <typename TYPE>
int sortList(kelvinsNode<TYPE>* &head){

    int ct = getCount(head);
    kelvinsNode<TYPE>* tempNode = head;
    kelvinsNode<TYPE>* newNode = NULL;
    for (int i = 0; i < ct; i++){
        insertSorted(newNode, tempNode->getCurrentValue());
        tempNode = tempNode->getNextAddress();
    }
    while(head->getNextAddress() != NULL){

        deleteNode(head, head);
    }
    head = newNode;
    return 1;
}

template <typename TYPE>
int deleteRepeats(kelvinsNode<TYPE>* &head){

    TYPE testVal = head->getCurrentValue();
    kelvinsNode<TYPE>* nodeTempTest = head;


    while (nodeTempTest != NULL){

        kelvinsNode<TYPE>* tempTraverse = nodeTempTest->getNextAddress();

        while(tempTraverse != NULL){

            if (testVal == tempTraverse->getCurrentValue()){
                kelvinsNode<TYPE>* nodeTemp = getNodeBefore(head, tempTraverse);
                deleteNode(head, tempTraverse);
                tempTraverse = nodeTemp;
            }
            tempTraverse = tempTraverse->getNextAddress();

        }

        nodeTempTest = nodeTempTest->getNextAddress();
        if (nodeTempTest != NULL){
             testVal = nodeTempTest->getCurrentValue();
        }
    }
}

template <typename TYPE>
int reverseList(kelvinsNode<TYPE>* &head){

    kelvinsNode<TYPE>* oldList = head;
    kelvinsNode<TYPE>* newList = NULL;
    kelvinsNode<TYPE>* lastNodePtr = head;
    int countOldList = getCount(oldList);

    while (lastNodePtr->getNextAddress() != NULL){
        lastNodePtr = lastNodePtr->getNextAddress();
    }
    insertHead(newList, lastNodePtr->getCurrentValue());

    lastNodePtr = head;
    for (int i = 0; i < countOldList-1; i++){

        insertAfter(newList, newList, lastNodePtr->getCurrentValue());
        lastNodePtr = lastNodePtr->getNextAddress();
    }

    while(oldList->getNextAddress() != NULL){

        deleteNode(oldList, oldList);
    }

    head = newList;
    return 0;
}

template <typename TYPE>
int getCount(kelvinsNode<TYPE>* head){

    int count = 0;
    kelvinsNode<TYPE>* nodeTemp = head;
    if (head == NULL){
        return 0;
    }
    while (nodeTemp->getNextAddress() != NULL){
        nodeTemp = nodeTemp->getNextAddress();
        ++count;
    }
    return count+1;
}

template <typename TYPE>
kelvinsNode<TYPE>* At(kelvinsNode<TYPE>* head, int count){

    kelvinsNode<TYPE>* tempNode = head;
    if (count < 0){
        return NULL;
    }
    for (int i = 0; i < count && (tempNode != NULL); i++){
        tempNode = tempNode->getNextAddress();
    }
    return tempNode;
}

template <typename TYPE>
int insertHead( kelvinsNode<TYPE>* &head, TYPE item){

    kelvinsNode<TYPE> *tempNodePtr = new kelvinsNode<TYPE>(item);
    tempNodePtr->setNextAddress(head);
    head = tempNodePtr;

    return 1;
}


template <typename TYPE>
int insertAfter( kelvinsNode<TYPE>* head, kelvinsNode<TYPE> *afterThis, TYPE item){

    kelvinsNode<TYPE> *tempNodePtr = new kelvinsNode<TYPE>(item);
    tempNodePtr->setNextAddress( afterThis->getNextAddress() );
    afterThis->setNextAddress( tempNodePtr->getCurrentAddress() );

     return 1;
}
template <typename TYPE>
int insertBefore(kelvinsNode<TYPE>* head, kelvinsNode<TYPE>* beforeThis, TYPE item){

    kelvinsNode<TYPE> *tempNodeBefore = getNodeBefore(head, beforeThis);
    insertAfter(head, tempNodeBefore, item);

    return 1;
}
template <typename TYPE, typename FUNC = ascendingOrder<TYPE> >
int insertSorted(kelvinsNode<TYPE>* &head, TYPE item, FUNC _function = ascendingOrder<TYPE>() ){

    kelvinsNode<TYPE>* tempNodeHead = head;

    if (head == NULL){
        insertHead(head, item);
        return 1;
    }else if (_function(head->getCurrentValue(), item) ){

        while (_function(tempNodeHead->getCurrentValue(), item) && tempNodeHead->getNextAddress() != NULL){

            tempNodeHead = tempNodeHead->getNextAddress();
        }
        if (_function(tempNodeHead->getCurrentValue(), item) || tempNodeHead->getCurrentValue() == item ){

            insertAfter(head, tempNodeHead, item);
        }else{

            insertBefore(head, tempNodeHead, item);
        }
        return 1;
    }else /*(head->getCurrentValue() >= item)*/ {

          insertHead(head, item);
          return 1;
    }
    return 0;//error
}





template <typename TYPE>
kelvinsNode<TYPE>* searchNode(kelvinsNode<TYPE>* head, TYPE item){

    kelvinsNode<TYPE> *xPtr = head;

    while(xPtr != NULL) {
        if (xPtr->getCurrentValue() == item){
            return xPtr;
        }
        xPtr = xPtr->getNextAddress();
    }

    return NULL;
}

template <typename TYPE>
int deleteNode( kelvinsNode<TYPE>* &head, kelvinsNode<TYPE> *deleteThis){

    //First need to find node before deleteThis..


    //if node to be deleted is the head node..
    if (deleteThis == head){

        head = deleteThis->getNextAddress();
        delete deleteThis;
        return 1;

    }else {// if node to be deleted is not the head node

        kelvinsNode<TYPE> *tempNodeBeforePtr = getNodeBefore(head, deleteThis);

        tempNodeBeforePtr->setNextAddress( deleteThis->getNextAddress() );
        delete deleteThis;
        return 1;
    }
    return 0;

}

template <typename TYPE>
int deleteItem(kelvinsNode<TYPE>* &head, TYPE value){

    //First need to find node before deleteThis..

    kelvinsNode<TYPE>* nodeToDelete = searchNode(head, value);
    if (nodeToDelete->getCurrentAddress() == NULL){
        return 0; //Delete operation failed
    }else{
        deleteNode(head, nodeToDelete);
        return 1;
    }
    return 0;
}

template <typename TYPE>
kelvinsNode<TYPE>* getNodeBefore(kelvinsNode<TYPE>* head, kelvinsNode<TYPE>* beforeThis){

    kelvinsNode<TYPE> *tempNodeBeforePtr;
    tempNodeBeforePtr = head;
    while ( tempNodeBeforePtr->getNextAddress() != beforeThis->getCurrentAddress()){

        tempNodeBeforePtr = tempNodeBeforePtr->getNextAddress();
    }
    return tempNodeBeforePtr;

}

template <typename TYPE>
int printList(kelvinsNode<TYPE>* head, string delimiter){

    kelvinsNode<TYPE>* tempNodePtr = head;

    while (tempNodePtr != NULL){
        cout << tempNodePtr->getCurrentValue() << delimiter;
        tempNodePtr = tempNodePtr->getNextAddress();
    }

    return 1;
}




#endif // _KELVINS_LIST_HELPERS_H_
