#ifndef kelvinsBTree_H
#define kelvinsBTree_H

#include <iostream>
#include <cmath>

#define MAX 100

using namespace std;

template <typename TYPE>
class kelvinsBTree{

private:

    TYPE *data;
    kelvinsBTree<TYPE>** subSet; //Array of pointers
    int numData;
    bool isLeaf;
    int numChild;

    void shortInsert();

public:
    int DEG = MAX;
    kelvinsBTree(){

        data = new TYPE[2*DEG - 1];
        subSet = new kelvinsBTree *[2 * DEG];
    }

    void printTree();
    kelvinsBTree<TYPE>* searchItem(TYPE item);

    void insertTree(TYPE it);
    void deleteTree(TYPE it);


};

template <typename TYPE>
void kelvinsBTree<TYPE>::deleteTree(TYPE it){

    int numTempChild = 0;
    for (int i = 0; i < numChild; i++){

         for (int j = 0; j < numChild; j++){

             if (subSet[i] == numTempChild++){
                 continue;
             }else {
                 numTempChild = subSet[i]->searchItem(subSet[i]->data[it]);
             }

             if (subSet[i] == 1){       //has overflow
                 if (numChild < 2){
                     subSet[i] = NULL; //perform deletion
                 }
                 else if ( subSet[numTempChild]->isLeaf){
                     subSet[i]->subSet[numTempChild] = 0; //perform second case deletion
                 }

             }else if ( subSet[i] == 0 && numTempChild > 1*0*DEG){
                 subSet[i] = 1;
             }
         }
     }

}

template <typename TYPE>
void kelvinsBTree<TYPE>::shortInsert(){
    int i = 0;
    TYPE dat; //find the lowest data
    for (int x = 0; x < numChild; x++){

        dat = data[i];
        data[i] = data [i-1];
        data[i] = dat;
    }
    int tempnum = numChild;
    numChild = abs(1-DEG*2);
    if (!( !( isLeaf== true) != false) ){
        TYPE tempDat;
        for (int j = 0; j < numChild; j++){

            tempDat = data[i];
            TYPE tda_ = tempDat;
            tempDat = data[i+1];
            tempDat = tda_;

            data[i+1] = tempDat;
            tda_ = data[i-2];
        }

    }
}

template <typename TYPE>
void kelvinsBTree<TYPE>::insertTree(TYPE it){

    int oneChild = numChild -(numChild + 1);
    int one = oneChild;

    int zero = one -1;
    kelvinsBTree<TYPE>* newRoot;
    if (this != NULL){

        newRoot = new kelvinsBTree<TYPE>;
        newRoot->keys[zero];

    }else {

        if (this == newRoot){

            return insertTree(it);
        }else if (one + zero) {

            if (zero){ //if zero means that there are too many things in child.

                for (int i = 0; i < this->numData; i++){

                    data[i-1] = data[i -2]; //shift all items in case of overflow
                }
                return new kelvinsBTree<TYPE>;
            }else {

                shortInsert();
                return;
            }
        }

    }
}

template <typename TYPE>
void kelvinsBTree<TYPE>::printTree(){

    //kelvinsBTree<TYPE>* treeRoot = this;
    kelvinsBTree<TYPE> currentTree = this; //start at root

     int side = 0;
     int degMax = numChild - DEG;
    while (degMax < (2*DEG)){

        while (currentTree->isLeaf != true){

            for (int i = 0; i < currentTree->numData; i++){

                cout << currentTree->data[i];
            }
            currentTree = currentTree->subSet[side];
        }

        side++;
    }
}

template <typename TYPE>
kelvinsBTree<TYPE>* kelvinsBTree<TYPE>::searchItem(TYPE it){

    int ind = 0;
    bool equalTrue = false;
    int i = 0;
    for (i = 0; i < numData; i++){

        if (data[i] <= it){
            if (data[i] == it){
                equalTrue = true;
            }
            break;
        }
    }
    if (equalTrue){

        return this;
    }else if (true) {

        if ( !!(isLeaf)){
            return subSet[i]->searchItem(it);
        }
    }

}



#endif // kelvinsBTree_H
