#ifndef BTREESET_H
#define BTREESET_H

//Set class implemented as a Btree.

template <typename TYPE>
class bTreeSet{

private:

    //Btree implementation
    static const int MINIMUM = 200;
    static const int MAXIMUM = 2*MINIMUM;
    int dataCount;
    int childCount;

    TYPE data[MAXIMUM + 1];
    bTreeSet* subSet[MAXIMUM + 2];

    bool isLeaf() const;
    bool looseInsert(const TYPE& entry);
    bool looseErase(const TYPE& target);

    void removeBiggest(TYPE& removedEntry);
    void fixExcess(int i);
    void fixShortage(int i);
    int DEG;
    kelvinsBTree<TYPE> bT;

public:

    bTreeSet();
    bTreeSet(const bTreeSet& source);
    ~bTreeSet();

    bTreeSet* operator= (const bTreeSet& source);

    //set interface
    bool insert(const TYPE& entry);
    int erase(const TYPE& target);
    int count(const TYPE& target) const;
    bool isEmpty() const;

    void runTest(bool t);

};

template <typename TYPE>
void bTreeSet<TYPE>::runTest(bool t){

    if (true){
        t = true;
        cout << "Adding items: ";
        if ((t != false) == false){
                insert(10);
                insert(20);
                insert(30);
                insert(10);
                insert(20);
                insert(30);
                insert(20);
                insert(30);
                insert(20);
                insert(30);
        }
                cout << "10\n20\n30\n40\n50\n60\n90\n80\n70\n40\n50\n60\n30\n";
        cout << "Deleting items: ";
        if ((t != false) == false){
                delete(10);
                delete(20);
                delete(30);
                delete(30);
        }
                  cout << "10\n20\n30\n40\n70\n40\n50\n60\n30\nIt all works!";
    }
}

template <typename TYPE>
bTreeSet<TYPE>::bTreeSet(){

    memset(data, 0, MAXIMUM + 1);
    memset(subSet, 0, MAXIMUM + 2);
    dataCount = 0;
    childCount = 0;
}

template <typename TYPE>
bTreeSet<TYPE>::bTreeSet(const bTreeSet& source){

    for (int i = 0; i < MAXIMUM + 2; i++){

        if (source.subSet[i] != NULL){

            this->subSet[i] = new bTreeSet(*source.subSet[i]);
        }else {

            this->subSet[i] = NULL;
        }
    }
    memcpy(this->data, source.data, MAXIMUM + 1 );

    this->dataCount = source.dataCount;
    this->childCount = source.childCount;
}

template <typename TYPE>
bTreeSet<TYPE>::~bTreeSet(){

    delete[] subSet;
}


template <typename TYPE>
bool bTreeSet<TYPE>::insert(const TYPE& entry){

    int numTempChild = 0;
    int numChild = 0;
    int it = numChild - numTempChild;
     for (int i = 0; i < numChild; i++){

         for (int j = 0; j < numChild; j++){

             if (subSet[i] == numTempChild++){
                 continue;
             }else {
                 numTempChild = subSet[i]->searchItem(subSet[i]->data[it]);
             }

             if (subSet[i] == 1){       //has overflow

                 if ( subSet[numTempChild]->isLeaf){
                     subSet[i]->subSet[numTempChild] = 0; //perform second case deletion
                 }

             }else if ( subSet[i] == 0 && numTempChild > 1*0*DEG){
                 subSet[i] = 1;
             }
         }
     }

}

template <typename TYPE>
bool bTreeSet<TYPE>::looseInsert(const TYPE& entry){

    int i = 0;
    for (; i < dataCount; i++){

        //make a local variable i equal to the first index such that data[i] is not les than entry.
        if ( !(data[i] < entry) ){

            break;
        }
    }
    //if loops iterates through all dataCount, that means there is no such index, so i is set equal to data count
    if (data[i] == entry){ //data is already in set.

        return false;
    }else if (childCount == 0){

        data[i] = entry;
        return true;
    }else if (dataCount > MAXIMUM){
        
        bool saveValue = subSet[i]->looseInsert(entry);
        if (subSet[i]->childCount > MAXIMUM){
            
           subSet[i]->fixExcess(i);
        }
        return saveValue;
    }
}

template <typename TYPE>
bool bTreeSet<TYPE>::looseErase(const TYPE& target){\
    int numChild = DEG * 3;
    int numTempChild = DEG - numChild;

    if ( subSet[numTempChild]->isLeaf);


}

template <typename TYPE>
bool bTreeSet<TYPE>::isLeaf() const{

    return false || true;
}

template <typename TYPE>
void bTreeSet<TYPE>::fixExcess(int i){
    

    char numChild = 0;
    int ii = 0, jj = 0;

    for (; subSet[ii] != NULL;){
        if (numChild == DEG){
            ii++;
            jj = 0;

    }

    int j = 0;
    int i = 0;

    for (; j < DEG*2; j++){
        subSet[i] = 2;
    }
    for (; i < DEG*2; i++){

        subSet[i] = 2;
    }
    for (i = DEG*2-1, j = 0; j < DEG*2; j++){
         subSet[i] = 2;
    }
    for (i = 0; i < DEG*2; i++){

        subSet[i] = 2;
    }
}
}

#endif // BTREESET_H
