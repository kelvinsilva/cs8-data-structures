#include <iostream>
#include "kelvinsBinTree.h"

using namespace std;

int main(){

//Print out contents of tree, tree traversal, etc...

    kelvinsBinTree<int> tree(100);
    tree.createFirstNode(10);
    tree.shiftToRoot();
    cout <<"       " <<tree.retrieve();
    tree.addLeft(15);
    tree.shiftLeft();
    cout << "\n" <<tree.retrieve();
    tree.shiftUp();
    tree.addRight(20);
    tree.shiftRight();
    cout << "               "<< tree.retrieve();

    tree.shiftToRoot();
    tree.change(222);
    cout << "\n\n\nPerforming change root to 222...\n\n";
    tree.shiftToRoot();
    cout <<"       " <<tree.retrieve();
    tree.addLeft(15);
    tree.shiftLeft();
    cout << "\n" <<tree.retrieve();
    tree.shiftUp();
    tree.addRight(20);
    tree.shiftRight();
    cout << "               "<< tree.retrieve();

    cin.get();
    return 0;
}
