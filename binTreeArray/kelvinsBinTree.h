#ifndef _KELVINS_BIN_TREE__H_
#define _KELVINS_BIN_TREE__H_

#include <iostream>
#include <vector>
#include <stdexcept>

using namespace std;

/*
kelvins binary tree implemented with stl vectors.
To use the class you  must put a maximum size of the internal vector to be used.
make sure to always addright or addleft before shifting. or else the program will throw a domain error.
*/

template <typename TYPE>
class kelvinsBinTree{

private:

    vector<TYPE> binTreeArray;
    vector<bool> isPresent;

    int treeSize;
    int currentNodeIndex;

public:

    kelvinsBinTree(int sz);

    //This function used to get a pointer to the internal vector,
    //so that if i need to print, manipulate or extend the vector.
    vector<TYPE>* getTreeContainer();

    void createFirstNode(const TYPE& item);
    void change(const TYPE& newEntry);

    void shiftToRoot();
    void shiftUp();
    void shiftLeft();
    void shiftRight();

    void addLeft(const TYPE& entry);
    void addRight(const TYPE& entry);

    bool hasParent();
    bool hasLeftChild();
    bool hasRightChild();

    int sizeTree();
    TYPE retrieve();

};

template <typename TYPE>
kelvinsBinTree<TYPE>::kelvinsBinTree(int sz){

    binTreeArray.resize(sz);
    isPresent.resize(sz);
    treeSize = 0;
    currentNodeIndex = 0;
}

    //This function used to get a pointer to the internal vector,
    //so that if i need to print, manipulate or extend the vector.
template <typename TYPE>
vector<TYPE>* kelvinsBinTree<TYPE>::getTreeContainer(){

    return &binTreeArray;
}

template <typename TYPE>
int kelvinsBinTree<TYPE>::sizeTree(){

    return treeSize;
}

template <typename TYPE>
bool kelvinsBinTree<TYPE>::hasParent(){

    return ( sizeTree() > 0 && isPresent[ ((currentNodeIndex - 1) / 2) ] == true );
}

template <typename TYPE>
bool kelvinsBinTree<TYPE>::hasLeftChild(){

    return ( sizeTree() > 0 && isPresent[ ((2 * currentNodeIndex) + 1) ] == true );
}

template <typename TYPE>
bool kelvinsBinTree<TYPE>::hasRightChild(){

    return ( sizeTree() > 0 && isPresent[ ((2* currentNodeIndex) + 2) ] == true );
}

template <typename TYPE>
void kelvinsBinTree<TYPE>::createFirstNode(const TYPE& item){

    if (sizeTree() == 0){

        binTreeArray[0] = item;
        isPresent[0] = true;
        ++treeSize;
        currentNodeIndex = 0;
    }
}

template <typename TYPE>
void kelvinsBinTree<TYPE>::shiftToRoot(){

    if (sizeTree() > 0){

        currentNodeIndex = 0;
    }
}

template <typename TYPE>
void kelvinsBinTree<TYPE>::shiftUp(){

    if (hasParent() == true){

        currentNodeIndex =  ( (currentNodeIndex - 1) / 2 );
    }
}

template <typename TYPE>
void kelvinsBinTree<TYPE>::shiftLeft(){

    if (hasLeftChild() == true){

        currentNodeIndex = ((2 * currentNodeIndex) + 1);
    }else {

        throw domain_error(string ("Current node has no left child, use addLeft() before this call to instantiate a child"));
    }
}

template <typename TYPE>
void kelvinsBinTree<TYPE>::shiftRight(){

    if (hasRightChild() == true){

        currentNodeIndex = ((2 * currentNodeIndex) + 2);
    }else {

        throw domain_error(string ("Current node has no right child, use addRight() before this call to instantiate a child"));
    }
}

template <typename TYPE>
void kelvinsBinTree<TYPE>::change(const TYPE& newEntry){

    if (sizeTree() > 0){

        binTreeArray[currentNodeIndex] = newEntry;
        isPresent[currentNodeIndex] = true;
        ++treeSize;
    }
}

template <typename TYPE>
void kelvinsBinTree<TYPE>::addLeft(const TYPE& entry){

    if (sizeTree() > 0 && hasLeftChild() == false){

        binTreeArray[ ((2 * currentNodeIndex) + 1 ) ] = entry;
        isPresent[ ((2 * currentNodeIndex) + 1 ) ] = true;
        ++treeSize;
    }
}

template <typename TYPE>
void kelvinsBinTree<TYPE>::addRight(const TYPE& entry){

    if (sizeTree() > 0 && hasRightChild() == false){

        binTreeArray[ ((2 * currentNodeIndex) + 2 ) ] = entry;
        isPresent[ ((2 * currentNodeIndex) + 2 ) ] = true;
        ++treeSize;
    }
}

template <typename TYPE>
TYPE kelvinsBinTree<TYPE>::retrieve(){

    if (sizeTree() > 0 && isPresent[currentNodeIndex]){
        return binTreeArray[currentNodeIndex];
    }
}

#endif // _KELVINS_BIN_TREE__H_
