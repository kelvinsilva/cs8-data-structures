Various data containers implemented using linked lists.
Kelvins lists are as follows:

kelvinsFrequency: Insert items into a list, count occurences of equivalent items inserted.

kelvinsList: Basic list, works like C++ vectors

kelvinsListHelpers: helper functions that deal with linked list manipulation. Used in class implementation

kelvinsMap: Works like C++ map class

kelvinsNode: Low level structure for linked list implementation.

kelvinsPair: Low level structure used in frequency, and map

kelvinsQueue: Queue container, FIFO, like C++ queue

kelvinsSortedList: A list that sorts items as they are inserted. Uses relational operators.

kelvinsStack: LIFO strucutre, work like C++ stack class

kelvinsUniqueList: A list container that only inserts items that do not exist in the list already.

kelvinsUniqueSortedList: A list ocntainer that only inserts items that do not exist in the list already, and sorts items using relational operators.

kelvinsPriorityQueue: A queue that is sorted using relational operators.





