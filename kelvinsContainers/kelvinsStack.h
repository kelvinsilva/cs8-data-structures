#ifndef _KELVINS_STACK_H_
#define _KELVINS_STACK_H_

#include <iostream>
#include "kelvinsNode.h"
#include "kelvinsListHelpers.h"

using namespace std;


template <typename TYPE>
class kelvinsStack{

private:

    kelvinsNode<TYPE>* nodeHead;
    int count;

public:

    kelvinsStack();
    ~kelvinsStack();

    int push(TYPE item);
    int pop();

    TYPE top();

    int getCount();

    int printStack();

};

template <typename TYPE>
kelvinsStack<TYPE>::kelvinsStack(){

    nodeHead = NULL;
    count = 0;
}

template <typename TYPE>
kelvinsStack<TYPE>::~kelvinsStack(){

    deleteList(nodeHead);
}

template <typename TYPE>
int kelvinsStack<TYPE>::push(TYPE item){

    insertHead(nodeHead, item);
    ++count;
}

template <typename TYPE>
int kelvinsStack<TYPE>::getCount(){

    return count;
}

template <typename TYPE>
int kelvinsStack<TYPE>::pop(){

    deleteNode(nodeHead, nodeHead);
    --count;
}

template <typename TYPE>
TYPE kelvinsStack<TYPE>::top(){

    return nodeHead->getCurrentValue();
}

template <typename TYPE>
int kelvinsStack<TYPE>::printStack(){

    printList(nodeHead, "\n");
}

#endif // _KELVINS_STACK_H_
