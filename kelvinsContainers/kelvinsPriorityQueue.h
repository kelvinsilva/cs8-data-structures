#ifndef _KELVINS_PRIORITY_QUEUE_H_
#define _KELVINS_PRIORITY_QUEUE_H_

#include "kelvinsNode.h"
#include "kelvinsPair.h"
#include "kelvinsListHelpers.h"
//TYPE is the value corresponding to the priority
//PRIORITY_TYPE is the type of the priority (can be ints, floats, strings, etc) that will be compared by the PRIORITY_COMPARATOR
//PRIORITY_COMPARATOR is the functor used to compare the priority item based on PRIORITY_TYPE
//Note that the PRIORITY_COMPARATOR must take in the same type as PRIORITY_TYPE

//Note that when instatiating the kelvinsPriorityQueue, your comparator must be able to compare a kelvinsPair<PRIORITY_TYPE, TYPE> type variable.
template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR = descendingOrder<kelvinsPair<PRIORITY_TYPE, TYPE> > >
class kelvinsPriorityQueue{

private:

    kelvinsNode<kelvinsPair<PRIORITY_TYPE, TYPE> >* listHead;
    int count;

public:

    kelvinsPriorityQueue();
    ~kelvinsPriorityQueue();

    void pushWithPriority(TYPE item, PRIORITY_TYPE priority);
    void pop();

    kelvinsPair<PRIORITY_TYPE, TYPE> topWithPriority();
    TYPE top();

    TYPE* getAddressOfItemAtIndex(int ind);

    bool isEmpty();
    int getPriorityQueueSize();

    void printPriorityQueue();

};

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR >
kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR >::kelvinsPriorityQueue(){

    listHead = NULL;
    count = 0;
}

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR >
kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR >::~kelvinsPriorityQueue(){

    deleteList(listHead);
}

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR>
void kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR>::pushWithPriority(TYPE item, PRIORITY_TYPE priority){

    kelvinsPair<PRIORITY_TYPE, TYPE> tempPair;
    tempPair.left = priority;
    tempPair.right = item;
    insertSorted(listHead, tempPair, PRIORITY_COMPARATOR());
    ++count;
}

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR>
void kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR>::pop(){

    if (count > 0){
        deleteNode(listHead, listHead);
        --count;
    }
}

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR>
TYPE kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR>::top(){

    kelvinsPair<PRIORITY_TYPE, TYPE> tempPair = listHead->getCurrentValue();
    return tempPair.right;
}

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR>
kelvinsPair<PRIORITY_TYPE, TYPE> kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR>::topWithPriority(){

    kelvinsPair<PRIORITY_TYPE, TYPE> tempPair = listHead->getCurrentValue();
    return tempPair;
}

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR >
bool kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR >::isEmpty(){

    return (count == 0);
}

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR >
TYPE* kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR >::getAddressOfItemAtIndex(int ind){

    if (ind >= 0 && ind < count){

        kelvinsNode<kelvinsPair<PRIORITY_TYPE, TYPE> >* tempNode = At(listHead, ind);
        TYPE* tempPtr = &(tempNode->getCurrentValueReference().right);
        return tempPtr;
    }else {

        return NULL;
    }
}

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR >
int kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR >::getPriorityQueueSize(){

    return count;
}

template <typename TYPE, typename PRIORITY_TYPE, typename PRIORITY_COMPARATOR >
void kelvinsPriorityQueue<TYPE, PRIORITY_TYPE, PRIORITY_COMPARATOR >::printPriorityQueue(){

    printList(listHead, "\n");
}

#endif // _KELVINS_PRIORITY_QUEUE_H_
