#include <iostream>
#include "kelvinsQueue.h"
#include "kelvinsFrequency.h"
#include "kelvinsList.h"
#include "kelvinsMap.h"
#include "kelvinsSortedList.h"
#include "kelvinsStack.h"
#include "kelvinsPriorityQueue.h"
#include "kelvinsListHelpers.h"
#include "kelvinsPair.h"

using namespace std;

int main()
{
    //Each list must go through a test.
    //The test consists of inserting 3 items into the list, and printing all 5 items in the list
    //Then deleting 1 item from the list and printing contents again.

    cout << "Testing kelvinsPriorityQueue:\n";
    kelvinsPriorityQueue<string, int > myPriority;
    myPriority.pushWithPriority("hey jude", 2);
    myPriority.pushWithPriority("sup man", 3);
    myPriority.pushWithPriority("foox bax", 100);
    cout << "\n\nPriority top: " << myPriority.top() << endl;
    cout << "\n\nPriority contents:\n";
    myPriority.printPriorityQueue();
    myPriority.pop();
    cout << "\n\nPriority contents, top popped:\n";
    myPriority.printPriorityQueue();

    cout << "\n\nTesting kelvinsQueue:";
    kelvinsQueue<int> myQueue;
    myQueue.pushBack(10);
    myQueue.pushBack(12);
    myQueue.pushBack(13);
    cout << "\nQueue contents:";
    myQueue.printQueue();
    cout << "\nQueue Front: " << myQueue.front() << "\n" << "Queue Back: " << myQueue.back();
    myQueue.popFront();
    cout << "\nQueue contents, front deleted:";
    myQueue.printQueue();

    cout << "\n\nTesting kelvinsFrequency:";
    kelvinsFrequency<string> myFreq;
    myFreq.insertFrequency("sup man");
    myFreq.insertFrequency("sup man");
    myFreq.insertFrequency("hey bro");
    cout << "\nkelvinsFrequency contents:\n";
    myFreq.printFrequency();
    myFreq.deleteFrequency("sup man");
    cout << "\nkelvinsFrequency deleted 1 item. Contents:\n";
    myFreq.printFrequency();

    cout << "\n\nTesting kelvinsList:\n";
    kelvinsList<string> myList;
    myList.insertItem("hey");
    myList.insertItem("foo");
    myList.insertItem("baz");
    cout << "List contents: ";
    myList.printListEx();
    myList.deleteItem("foo");
    myList.deleteItemAt(1);
    myList.deleteItemAt(-999);
    cout << "\nList contents, 2 deleted: ";
    myList.printListEx();

    cout << "\n\nTesting kelvinsMap:\n";
    kelvinsMap<string, int> myMap;
    myMap["foo"] = 3;
    myMap.insertMap("baz", 4);
    myMap["123"] = 123;
    cout << "Map contents:\n";
    myMap.printMap();
    myMap.deletePair("123");
    cout << "\nMap contents, \"123\" deleted:\n";
    myMap.printMap();


    cout << "\n\nTesting kelvinsSortedList:\n";
    kelvinsSortedList<int> mySortedList;
    mySortedList.insertItem(10);
    mySortedList.insertItem(-100);
    mySortedList.insertItem(0);
    cout << "\nSorted List contents: ";
    mySortedList.printSortedList();
    mySortedList.deleteItem(0);
    mySortedList.deleteItemAt(0);
    cout << "\nSorted List \"0\" and \"-100\" deleted:\n";
    mySortedList.printSortedList();

    cout << "\n\nTesting kelvinsStack:\n\n";
    kelvinsStack<int> myStack;
    myStack.push(999);
    myStack.push(12225);
    myStack.push(0);
    cout << "\nStack contents:\n";
    myStack.printStack();
    cout << "\nTop of stack: " << myStack.top() << "\n";
    myStack.pop();
    cout << "Stack contents, top popped:\n";
    myStack.printStack();

    cin.get();
    return 0;
}
