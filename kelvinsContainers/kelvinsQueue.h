#ifndef _KELVINS_QUEUE_H_
#define _KELVINS_QUEUE_H_

#include <iostream>
#include "kelvinsListHelpers.h"
#include "kelvinsNode.h"

using namespace std;


template <typename T>
class kelvinsQueue{

private:

    kelvinsNode<T>* listHead;

public:

    kelvinsQueue();
    ~kelvinsQueue();

    T front();
    T back();

    void pushBack(T tp);
    void popFront();

    int getQueueSize();
    bool isEmpty();

    void printQueue();

};

template <typename T>
kelvinsQueue<T>::kelvinsQueue(){

    listHead = NULL;
}

template <typename T>
kelvinsQueue<T>::~kelvinsQueue(){

    deleteList(listHead);
}

template <typename T>
T kelvinsQueue<T>::front(){

    kelvinsNode<T>* tempNode = At(listHead, getCount(listHead)-1);
    return tempNode->getCurrentValue();
}

template <typename T>
T kelvinsQueue<T>::back(){

    return listHead->getCurrentValue();
}

template <typename T>
void kelvinsQueue<T>::pushBack(T tp){

    insertHead(listHead, tp);
}

template <typename T>
void kelvinsQueue<T>::popFront(){

    deleteNode(listHead, At(listHead, getCount(listHead)-1) );
}

template <typename T>
bool kelvinsQueue<T>::isEmpty(){

    return (getCount(listHead) == 0);
}

template <typename T>
int kelvinsQueue<T>::getQueueSize(){

    return getCount(listHead);
}

template <typename T>
void kelvinsQueue<T>::printQueue(){
    cout << " |Back: ";
    printList(listHead, " -> ");
    cout << ":Front| ";
}

#endif // _KELVINS_QUEUE_H_
