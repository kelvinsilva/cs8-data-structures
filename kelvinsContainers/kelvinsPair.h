#ifndef _KELVINS_PAIR_H_
#define _KELVINS_PAIR_H_


template <typename TYPE_ONE, typename TYPE_TWO>
struct kelvinsPair{

    TYPE_ONE left;  //key
    TYPE_TWO right; //value

    TYPE_ONE& getLeftReference();
    TYPE_TWO& getRightReference();

    bool operator<(const kelvinsPair<TYPE_ONE, TYPE_TWO>& rhs) const;
    bool operator>(const kelvinsPair<TYPE_ONE, TYPE_TWO>& rhs) const;

    bool operator>= (const kelvinsPair<TYPE_ONE, TYPE_TWO>& rhs) const;
    bool operator<= (const kelvinsPair<TYPE_ONE, TYPE_TWO>& rhs) const;

    bool operator==(const kelvinsPair<TYPE_ONE, TYPE_TWO>& rhs) const;

    kelvinsPair<TYPE_ONE, TYPE_TWO>& operator++(){
        ++right;
        return *this;
    }
    kelvinsPair<TYPE_ONE, TYPE_TWO> operator++(int){
        kelvinsPair<TYPE_ONE, TYPE_TWO> tmp(*this);
        operator++();
        return tmp;
    }

    template <typename RIGHT, typename LEFT>
    friend ostream& operator<<(ostream &out, const kelvinsPair<RIGHT, LEFT>& input);

};

template <typename RIGHT, typename LEFT>
bool kelvinsPair<RIGHT, LEFT>::operator>= (const kelvinsPair<RIGHT, LEFT>& rhs) const{

    return left >= rhs.left;
}

template <typename RIGHT, typename LEFT>
bool kelvinsPair<RIGHT, LEFT>::operator<= (const kelvinsPair<RIGHT, LEFT>& rhs) const{

    return left <= rhs.left;
}

template <typename RIGHT, typename LEFT>
ostream& operator<<(ostream &out, const kelvinsPair<RIGHT, LEFT>& input){

    out << "[" << input.left << "]" << " = " << input.right;
    return out;
}

template <typename TYPE_ONE, typename TYPE_TWO >
TYPE_ONE& kelvinsPair<TYPE_ONE, TYPE_TWO>::getLeftReference(){

    return left;
}

template <typename TYPE_ONE, typename TYPE_TWO >
TYPE_TWO& kelvinsPair<TYPE_ONE, TYPE_TWO>::getRightReference(){

    return right;
}

template <typename TYPE_ONE, typename TYPE_TWO >
bool kelvinsPair<TYPE_ONE, TYPE_TWO>::operator< (const kelvinsPair<TYPE_ONE, TYPE_TWO>& rhs) const {

    return left < rhs.left;

}
template <typename TYPE_ONE, typename TYPE_TWO >
bool kelvinsPair<TYPE_ONE, TYPE_TWO>::operator> (const kelvinsPair<TYPE_ONE, TYPE_TWO>& rhs) const {

    return left > rhs.left;
}

template <typename TYPE_ONE, typename TYPE_TWO >
bool kelvinsPair<TYPE_ONE, TYPE_TWO>::operator== (const kelvinsPair<TYPE_ONE, TYPE_TWO>& rhs) const {

    return left == rhs.left;
}

#endif // _KELVINS_PAIR_H_
