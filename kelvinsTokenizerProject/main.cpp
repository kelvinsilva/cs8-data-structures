#include <iostream>
#include "kelvinsLists/list.h"
#include "tokenizerEngine/tokenEngine.h"
#include "tokenizerEngineWrap/token.h"
#include "tokenizerEngineWrap/stokenize.h"
#include "tokenizerEngineWrap/ftokenize.h"
#include "kelvinsLists/Frequency.h"

using namespace std;


/*
 *Tokenizer Engine contains low level c-string manipulation functions
 *getNextToken in tokenizer engine is deprectaed, so a new one was written in stokenizer class
 *most functions from tokenizer engine are used in ftokenize and stokenize but not all of them
 *tokenizer engine wrap are classes that make use of tokenizer engine low level functions
 *go to: https://bitbucket.org/kelvinsilva/computer_science_pcc/src/2b3b44784c00f2b27fef95605aa18fb532536caf/CS2/file_tokenizer/diagram_documentation.png?at=master
 *for tokenizer engine function diagram
 *
 *kelvinsLists are other low level functions that helps to manage tokens.
*/

//Ftokenize takes in a text file and tokenizes
//Main function tokenizes each file and assigns to tok
//Can also use frequency class to count statistics for files
//but in my case, frequency class is super slow. I need to optimize more.
//600,000 word lorem ipsum dolor text file in under a second

int main()
{

    FTokenize myTokenizer("testFile.txt");
    Token tok;

    frequencyList<string> tokStatistics;

    int i = 0;
    while (myTokenizer.moreTokensStatus()){
        ++i;
        if (i >= 200){
            break;
        }
        tok = myTokenizer.extractNextToken();
        cout << tok << "\n";
    }

    cin.get();

    return 0;
}

