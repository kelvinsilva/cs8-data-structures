#ifndef UNIQUESORTEDLIST_H
#define UNIQUESORTEDLIST_H
#include "node.h"

//Work in Progress
template <typename TYPENAME>
class uniqueSortedList{

private:
    node<TYPE> headNode;
public:

    template <typename TYPE>
    int insertHead( node<TYPE>* &head, TYPE item);

    template <typename TYPE>
    int insertAfter( node<TYPE>* head, node<TYPE> *afterThis, TYPE item);

    template <typename TYPE>
    int insertBefore(node<TYPE>* head, node<TYPE>* beforeThis, TYPE item);

    template <typename TYPE>
    int insertSorted(node<TYPE>* &head, TYPE item);

    template <typename TYPE>
    int deleteNode( node<TYPE>* &head, node<TYPE> *deleteThis);

    template <typename TYPE>
    int deleteItem(node<TYPE>* &head, TYPE value);

    template <typename TYPE>
    node<TYPE>* searchNode(node<TYPE>* head, TYPE item);

    template <typename TYPE>
    int printList(node<TYPE>* head);

    template <typename TYPE>
    node<TYPE>* getNodeBefore(node<TYPE>* head, node<TYPE>* beforeThis);

};
template <typename TYPE>
int shuffleList(node<TYPE>* &head){

    srand(time(NULL));

    int randAt = rand()%getCount(head) - 1;
    node<TYPE>* newList = NULL;
    node<TYPE>* tempTraverse = head;
    node<TYPE>* tempInsert = At(head, randAt);
    node<TYPE>* tempTempInsert = tempInsert;

    insertHead(newList, tempInsert->getCurrentValue());
    tempInsert = tempInsert->getNextAddress();

    while (tempInsert != NULL){

        insertHead(newList, tempInsert->getCurrentValue());
        tempInsert = tempInsert->getNextAddress();
    }
    while(tempTraverse != tempTempInsert){

        insertHead(newList, tempTraverse->getCurrentValue());
        tempTraverse = tempTraverse->getNextAddress();
    }
    head = newList;
}

template <typename TYPE>
int sortList(node<TYPE>* &head){

    int ct = getCount(head);
    node<TYPE>* tempNode = head;
    node<TYPE>* newNode = NULL;
    for (int i = 0; i < ct; i++){
        insertSorted(newNode, tempNode->getCurrentValue());
        tempNode = tempNode->getNextAddress();
    }
    while(head->getNextAddress() != NULL){

        deleteNode(head, head);
    }
    head = newNode;
    return 1;
}

template <typename TYPE>
int deleteRepeats(node<TYPE>* &head){

    TYPE testVal = head->getCurrentValue();
    node<TYPE>* nodeTempTest = head;


    while (nodeTempTest != NULL){

        node<TYPE>* tempTraverse = nodeTempTest->getNextAddress();

        while(tempTraverse != NULL){

            if (testVal == tempTraverse->getCurrentValue()){
                node<TYPE>* nodeTemp = getNodeBefore(head, tempTraverse);
                deleteNode(head, tempTraverse);
                tempTraverse = nodeTemp;
            }
            tempTraverse = tempTraverse->getNextAddress();

        }

        nodeTempTest = nodeTempTest->getNextAddress();
        if (nodeTempTest != NULL){
             testVal = nodeTempTest->getCurrentValue();
        }
    }
}

template <typename TYPE>
int reverseList(node<TYPE>* &head){

    node<TYPE>* oldList = head;
    node<TYPE>* newList = NULL;
    node<TYPE>* lastNodePtr = head;
    int countOldList = getCount(oldList);

    while (lastNodePtr->getNextAddress() != NULL){
        lastNodePtr = lastNodePtr->getNextAddress();
    }
    insertHead(newList, lastNodePtr->getCurrentValue());

    lastNodePtr = head;
    for (int i = 0; i < countOldList-1; i++){

        insertAfter(newList, newList, lastNodePtr->getCurrentValue());
        lastNodePtr = lastNodePtr->getNextAddress();
    }

    while(oldList->getNextAddress() != NULL){

        deleteNode(oldList, oldList);
    }

    head = newList;
    return 0;
}

template <typename TYPE>
int getCount(node<TYPE>* head){

    int count = 0;
    node<TYPE>* nodeTemp = head;
    if (head == NULL){
        return 0;
    }
    while (nodeTemp->getNextAddress() != NULL){
        nodeTemp = nodeTemp->getNextAddress();
        ++count;
    }
    return count+1;
}

template <typename TYPE>
node<TYPE>* At(node<TYPE>* head, int count){
    node<TYPE>* tempNode = head;
    if (head == NULL){
        return head;
    }

    for (int i = 0; i < count && (tempNode->getNextAddress() != NULL); i++){
        tempNode = tempNode->getNextAddress();
    }
    return tempNode;
}

template <typename TYPE>
int insertHead( node<TYPE>* &head, TYPE item){

    node<TYPE> *tempNodePtr = new node<TYPE>(item);
    tempNodePtr->setNextAddress(head);
    head = tempNodePtr;

    return 1;
}


template <typename TYPE>
int insertAfter( node<TYPE>* head, node<TYPE> *afterThis, TYPE item){

    node<TYPE> *tempNodePtr = new node<TYPE>(item);
    tempNodePtr->setNextAddress( afterThis->getNextAddress() );
    afterThis->setNextAddress( tempNodePtr->getCurrentAddress() );

     return 1;
}
template <typename TYPE>
int insertBefore(node<TYPE>* head, node<TYPE>* beforeThis, TYPE item){

    node<TYPE> *tempNodeBefore = getNodeBefore(head, beforeThis);
    insertAfter(head, tempNodeBefore, item);

}
template <typename TYPE>
int insertSorted(node<TYPE>* &head, TYPE item){

    node<TYPE>* tempNodeHead = head;

    if (head == NULL){
        insertHead(head, item);
        return 1;
    }else if(head->getCurrentValue() >= item) {

        insertHead(head, item);
        return 1;
    }else if (head->getCurrentValue() < item){

        while (tempNodeHead->getCurrentValue() < item && tempNodeHead->getNextAddress() != NULL){

            tempNodeHead = tempNodeHead->getNextAddress();
        }

        if (tempNodeHead->getCurrentValue() <= item){

            insertAfter(head, tempNodeHead, item);
        }else{

            insertBefore(head, tempNodeHead, item);
        }

        return 1;
    }
    return 0;//error
}





template <typename TYPE>
node<TYPE>* searchNode(node<TYPE>* head, TYPE item){

    node<TYPE> *xPtr = head;

    while(xPtr->getCurrentValue() != item) {
        if (xPtr->getCurrentAddress() == NULL){
            return NULL;
        }
        xPtr = xPtr->getNextAddress();
    }

    return xPtr;
}

template <typename TYPE>
int deleteNode( node<TYPE>* &head, node<TYPE> *deleteThis){

    //First need to find node before deleteThis..


    //if node to be deleted is the head node..
    if (deleteThis == head){

        head = deleteThis->getNextAddress();
        delete deleteThis;
        return 1;

    }else {// if node to be deleted is not the head node

        node<TYPE> *tempNodeBeforePtr = getNodeBefore(head, deleteThis);

        tempNodeBeforePtr->setNextAddress( deleteThis->getNextAddress() );
        delete deleteThis;
        return 1;
    }
    return 0;

}

template <typename TYPE>
int deleteItem(node<TYPE>* &head, TYPE value){

    //First need to find node before deleteThis..

    node<TYPE>* nodeToDelete = searchNode(head, value);
    if (nodeToDelete->getCurrentAddress() == NULL){
        return 0; //Delete operation failed
    }else{
        deleteNode(head, nodeToDelete);
        return 1;
    }
    return 0;
}

template <typename TYPE>
node<TYPE>* getNodeBefore(node<TYPE>* head, node<TYPE>* beforeThis){

    node<TYPE> *tempNodeBeforePtr;
    tempNodeBeforePtr = head;
    while ( tempNodeBeforePtr->getNextAddress() != beforeThis->getCurrentAddress()){

        tempNodeBeforePtr = tempNodeBeforePtr->getNextAddress();
    }
    return tempNodeBeforePtr;

}

template <typename TYPE>
int printList(node<TYPE>* head){

    node<TYPE>* tempNodePtr = head;

    while (tempNodePtr != NULL){
        cout << tempNodePtr->getCurrentValue() << ", ";
        tempNodePtr = tempNodePtr->getNextAddress();
    }

#endif // UNIQUESORTEDLIST_H
