#ifndef SORTEDLIST_H
#define SORTEDLIST_H

#include "node.h"
#include "kelvinsL_List.h";


template <typename TYPE>
class sortedList{

private:

   node<TYPE> HeadNode;


public:


   void insertItem(TYPE item);
   void deleteItem(TYPE item);

   void reverse();
   void print();

   node<TYPE>* searchList(TYPE item);

   int getSize();

   template <typename T>
   friend ostream& operator<<(ostream &out, const list<T> &lst);

   template <typename T>
   friend istream& operator>>(istream &in, list<T> &lst);
}

template <typename TYPE>
void sortedList<TYPE>::insertItem(TYPE item){
    insertSorted(listHead)
}

template<typename TYPE>
void sortedList<TYPE>::deleteItem(TYPE item){
    deleteNode(listHead, search(listHead, item));
}

template <typename TYPE>
void sortedList<TYPE>::reverseList(){
    reverse(listHead);

}

template <typename TYPE>
void sortedList<TYPE>::print(){
    printList(listHead);
}

template <typename TYPE>
node<TYPE>* sortedList<TYPE>::searchList(TYPE item){
    return search(listHead, item);
}

template <typename TYPE>
int sortedList<TYPE>::getSize(){

    return getCount(listHead);
}

template <typename T>
ostream& operator<<(ostream &out, const list<T> &lst){

    node<T>* tempNode = lst.listHead;
    while (tempNode != NULL){
        out << tempNode->getCurrentValue() << "->";
        tempNode = tempNode->getNextAddress();
    }
    return out;
}

template <typename T>
istream& operator>>(istream &in, list<T> &lst){
    T tempTYPE;
    in >> tempTYPE;
    lst.insertItem(lst.listHead, tempTYPE);

    return in;
}

#endif // SORTEDLIST_H
