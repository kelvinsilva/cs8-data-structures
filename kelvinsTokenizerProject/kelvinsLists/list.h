#ifndef LIST_H
#define LIST_H

#include <iostream>
#include "kelvinsL_List.h"

#include "node.h"

using namespace std;

template <typename TYPE>
class list{

private:

    node<TYPE>* listHead;

public:

    list();
    list(node<TYPE>* no);

    void insertItem(TYPE item);
    void insertItem(TYPE item, int position);

    void deleteItem(TYPE item);

    void sort();
    void reverse();
    void print();

    void randomize();

    node<TYPE>* search(TYPE item);

    int getSize();

    template <typename T>
    friend ostream& operator<<(ostream &out, const list<T> &lst);

    template <typename T>
    friend istream& operator>>(istream &in, list<T> &lst);



};

template <typename TYPE>
node<TYPE>* list<TYPE>::search(TYPE item){

    return searchList(item);
}


template <typename TYPE>
list<TYPE>::list(){
    listHead = NULL;
}

template <typename TYPE>
list<TYPE>::list(node<TYPE>* no){
    listHead = no;
}

template <typename TYPE>
int list<TYPE>::getSize(){

    return getCount(listHead);
}

template <typename TYPE>
void list<TYPE>::insertItem(TYPE item){

    insertHead(listHead, item);
}

template <typename TYPE>
void list<TYPE>::insertItem(TYPE item, int position){

    if (position < this->getSize() - 1){
        if (position == 0){
            insertHead(listHead, item);
        }else{
            insertAfter(listHead, At(listHead, position), item);
        }
    }
}

template <typename TYPE>
void list<TYPE>::deleteItem(TYPE item){

    deleteNode(listHead, searchList(listHead, item));
}

template <typename TYPE>
void list<TYPE>::sort(){

    sortList(listHead);
}

template <typename TYPE>
void list<TYPE>::reverse(){

    reverseList(listHead);
}

template<typename TYPE>
void list<TYPE>::randomize(){

    shuffleList(listHead);
}

template <typename TYPE>
void list<TYPE>::print(){

    printList(listHead);
}

template <typename T>
ostream& operator<<(ostream &out, const list<T> &lst){

    node<T>* tempNode = lst.listHead;
    while (tempNode != NULL){
        out << tempNode->getCurrentValue() << "->";
        tempNode = tempNode->getNextAddress();
    }
    return out;
}

template <typename T>
istream& operator>>(istream &in, list<T> &lst){
    T tempTYPE;
    in >> tempTYPE;
    lst.insertItem(lst.listHead, tempTYPE);

    return in;
}

/*
void deleteItem(TYPE item);
void deleteItem(int position);

void sortList();
void printList();

node<TYPE>* searchList(TYPE item);

int getSize();

friend ostream& operator<<(ostream &out, const list<TYPE> &lst);
friend istream& operator>>(istream &in, list<TYPE> &lst);*/
#endif //LIST_H
