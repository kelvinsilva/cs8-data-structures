#ifndef KELVINSL_LIST_H
#define KELVINSL_LIST_H


#include <iostream>
#include <time.h>
#include <cstdlib>

//Kelvin da Silva
#include "node.h"

using namespace std;

template <typename TYPE_ONE>
class ascendingOrder{

public:
    bool operator()(TYPE_ONE left, TYPE_ONE right){
        return (left < right);
    }
};

template <typename TYPE_ONE>
class descendingOrder{

public:
    bool operator()(TYPE_ONE left, TYPE_ONE right){
        return (left > right);
    }

};

template <typename T>
class doNothing{

public:
    void operator()(T left){

    }
};

template <typename T>
class printMe{

public:
    void operator()(T& left){
        cout << left;
    }
};

template <typename T>
class addMe{

public:
    void operator()(T& left){
        ++left;
    }
};

//usage: _function specifies the ordering in which to sort, and _functionTwo applies a certain function to the value of the node if a collision exists.
template <typename TYPE, typename FUNC = ascendingOrder<TYPE>, typename FUNC_TWO = doNothing<TYPE> >
int insertUniqueSorted(node<TYPE>* &head, TYPE item, FUNC _function = ascendingOrder<TYPE>() , FUNC_TWO _functionTwo = doNothing<TYPE>() );

template <typename TYPE>
int insertHead( node<TYPE>* &head, TYPE item);

template <typename TYPE>
int insertAfter( node<TYPE>* head, node<TYPE> *afterThis, TYPE item);

template <typename TYPE>
int insertBefore(node<TYPE>* head, node<TYPE>* beforeThis, TYPE item);

template <typename TYPE, typename FUNC >
int insertSorted(node<TYPE>* &head, TYPE item, FUNC _function);

template <typename TYPE>
int deleteNode( node<TYPE>* &head, node<TYPE> *deleteThis);

template <typename TYPE>
int deleteItem(node<TYPE>* &head, TYPE value);

template <typename TYPE>
node<TYPE>* searchNode(node<TYPE>* head, TYPE item);

template <typename TYPE>
int printList(node<TYPE>* head, string delimiter = ", ");

template <typename TYPE>
node<TYPE>* getNodeBefore(node<TYPE>* head, node<TYPE>* beforeThis);

template <typename TYPE>
node<TYPE>* At(node<TYPE>* head, int count);

template <typename TYPE>
int getCount(node<TYPE>* head);

template <typename TYPE>
int reverseList(node<TYPE>* &head);

template <typename TYPE>
int deleteRepeats(node<TYPE>* &head);

template <typename TYPE>
int sortList(node<TYPE>* &head);

template <typename TYPE>
int shuffleList(node<TYPE>* &head);

template <typename TYPE>
int copyList(node<TYPE>* head, node<TYPE>* &copyToThis);

template <typename TYPE>
node<TYPE>* mergeList(node<TYPE>* listOne, node<TYPE>* listTwo);

template <typename TYPE>
node<TYPE>* uniqueMergeList(node<TYPE>* listOne, node<TYPE>* listTwo);

template <typename TYPE>
int splitList(node<TYPE>* head, node<TYPE>* at, node<TYPE>* &left, node<TYPE>* &right);




template <typename TYPE, typename FUNC = ascendingOrder<TYPE>, typename FUNC_TWO = doNothing<TYPE> >
int insertUniqueSorted(node<TYPE>* &head, TYPE item, FUNC _function = ascendingOrder<TYPE>() , FUNC_TWO _functionTwo = doNothing<TYPE>() ){

    node<TYPE>* tempUnique = searchNode(head, item);


    if (tempUnique == NULL){

        insertSorted(head, item, _function);
    }else {
        _functionTwo( tempUnique->getCurrentValueReference() );   //apply custom functor.
        return 1;
    }

}

template <typename TYPE>
int splitList(node<TYPE>* head, node<TYPE>* at, node<TYPE>* &left, node<TYPE>* &right){

    node<TYPE>* newList = NULL;
    node<TYPE>* tempTraverse = head;
    while (tempTraverse != at){

        insertHead(newList, tempTraverse->getCurrentValue() );
        tempTraverse = tempTraverse->getNextAddress();
    }
    reverseList(newList);
    left = newList;
    tempTraverse = at;
    newList = NULL;

    while(tempTraverse != NULL){

        insertHead(newList, tempTraverse->getCurrentValue());
        tempTraverse = tempTraverse->getNextAddress();
    }
    reverseList(newList);
    right = newList;

    return 1;

}

template <typename TYPE>
node<TYPE>* uniqueMergeList(node<TYPE>* listOne, node<TYPE>* listTwo){

    node<TYPE>* tempNode = mergeList(listOne, listTwo);
    deleteRepeats(tempNode);
    return tempNode;
}

template <typename TYPE>
node<TYPE>* mergeList(node<TYPE>* listOne, node<TYPE>* listTwo){

    node<TYPE>* newList = NULL;
    copyList(listOne, newList);
    int count = getCount(newList) - 1;
    node<TYPE>* tempPtr = At(newList, count);
    tempPtr->setNextAddress(listTwo);

    return newList;
}

template <typename TYPE>
int copyList(node<TYPE>* head, node<TYPE>* &copyToThis){

    node<TYPE>* tempTraverse = head;

    while(tempTraverse != NULL){

        insertHead(copyToThis, tempTraverse->getCurrentValue());
        tempTraverse = tempTraverse->getNextAddress();
    }
    reverseList(copyToThis);
}

template <typename TYPE>
int shuffleList(node<TYPE>* &head){

    srand(time(NULL));

    int randAt = rand()%getCount(head) - 1;
    node<TYPE>* newList = NULL;
    node<TYPE>* tempTraverse = head;
    node<TYPE>* tempInsert = At(head, randAt);
    node<TYPE>* tempTempInsert = tempInsert;

    insertHead(newList, tempInsert->getCurrentValue());
    tempInsert = tempInsert->getNextAddress();

    while (tempInsert != NULL){

        insertHead(newList, tempInsert->getCurrentValue());
        tempInsert = tempInsert->getNextAddress();
    }
    while(tempTraverse != tempTempInsert){

        insertHead(newList, tempTraverse->getCurrentValue());
        tempTraverse = tempTraverse->getNextAddress();
    }
    tempTraverse = head;
    while (tempTraverse != NULL){
        deleteNode(tempTraverse, tempTraverse);
    }
    head = newList;
}

template <typename TYPE>
int sortList(node<TYPE>* &head){

    int ct = getCount(head);
    node<TYPE>* tempNode = head;
    node<TYPE>* newNode = NULL;
    for (int i = 0; i < ct; i++){
        insertSorted(newNode, tempNode->getCurrentValue());
        tempNode = tempNode->getNextAddress();
    }
    while(head->getNextAddress() != NULL){

        deleteNode(head, head);
    }
    head = newNode;
    return 1;
}

template <typename TYPE>
int deleteRepeats(node<TYPE>* &head){

    TYPE testVal = head->getCurrentValue();
    node<TYPE>* nodeTempTest = head;


    while (nodeTempTest != NULL){

        node<TYPE>* tempTraverse = nodeTempTest->getNextAddress();

        while(tempTraverse != NULL){

            if (testVal == tempTraverse->getCurrentValue()){
                node<TYPE>* nodeTemp = getNodeBefore(head, tempTraverse);
                deleteNode(head, tempTraverse);
                tempTraverse = nodeTemp;
            }
            tempTraverse = tempTraverse->getNextAddress();

        }

        nodeTempTest = nodeTempTest->getNextAddress();
        if (nodeTempTest != NULL){
             testVal = nodeTempTest->getCurrentValue();
        }
    }
}

template <typename TYPE>
int reverseList(node<TYPE>* &head){

    node<TYPE>* oldList = head;
    node<TYPE>* newList = NULL;
    node<TYPE>* lastNodePtr = head;
    int countOldList = getCount(oldList);

    while (lastNodePtr->getNextAddress() != NULL){
        lastNodePtr = lastNodePtr->getNextAddress();
    }
    insertHead(newList, lastNodePtr->getCurrentValue());

    lastNodePtr = head;
    for (int i = 0; i < countOldList-1; i++){

        insertAfter(newList, newList, lastNodePtr->getCurrentValue());
        lastNodePtr = lastNodePtr->getNextAddress();
    }

    while(oldList->getNextAddress() != NULL){

        deleteNode(oldList, oldList);
    }

    head = newList;
    return 0;
}

template <typename TYPE>
int getCount(node<TYPE>* head){

    int count = 0;
    node<TYPE>* nodeTemp = head;
    if (head == NULL){
        return 0;
    }
    while (nodeTemp->getNextAddress() != NULL){
        nodeTemp = nodeTemp->getNextAddress();
        ++count;
    }
    return count+1;
}

template <typename TYPE>
node<TYPE>* At(node<TYPE>* head, int count){
    node<TYPE>* tempNode = head;
    if (head == NULL){
        return head;
    }

    for (int i = 0; i < count && (tempNode != NULL); i++){
        tempNode = tempNode->getNextAddress();
    }
    return tempNode;
}

template <typename TYPE>
int insertHead( node<TYPE>* &head, TYPE item){

    node<TYPE> *tempNodePtr = new node<TYPE>(item);
    tempNodePtr->setNextAddress(head);
    head = tempNodePtr;

    return 1;
}


template <typename TYPE>
int insertAfter( node<TYPE>* head, node<TYPE> *afterThis, TYPE item){

    node<TYPE> *tempNodePtr = new node<TYPE>(item);
    tempNodePtr->setNextAddress( afterThis->getNextAddress() );
    afterThis->setNextAddress( tempNodePtr->getCurrentAddress() );

     return 1;
}
template <typename TYPE>
int insertBefore(node<TYPE>* head, node<TYPE>* beforeThis, TYPE item){

    node<TYPE> *tempNodeBefore = getNodeBefore(head, beforeThis);
    insertAfter(head, tempNodeBefore, item);

}
template <typename TYPE, typename FUNC >
int insertSorted(node<TYPE>* &head, TYPE item, FUNC _function){

    node<TYPE>* tempNodeHead = head;

    if (head == NULL){
        insertHead(head, item);
        return 1;
    }else if (_function(head->getCurrentValue(), item) ){

        while (_function(tempNodeHead->getCurrentValue(), item) && tempNodeHead->getNextAddress() != NULL){

            tempNodeHead = tempNodeHead->getNextAddress();
        }
        if (_function(tempNodeHead->getCurrentValue(), item) || tempNodeHead->getCurrentValue() == item ){

            insertAfter(head, tempNodeHead, item);
        }else{

            insertBefore(head, tempNodeHead, item);
        }
        return 1;
    }else /*(head->getCurrentValue() >= item)*/ {

          insertHead(head, item);
          return 1;
    }
    return 0;//error
}





template <typename TYPE>
node<TYPE>* searchNode(node<TYPE>* head, TYPE item){

    node<TYPE> *xPtr = head;

    while(xPtr != NULL) {
        if (xPtr->getCurrentValue() == item){
            return xPtr;
        }
        xPtr = xPtr->getNextAddress();
    }

    return NULL;
}

template <typename TYPE>
int deleteNode( node<TYPE>* &head, node<TYPE> *deleteThis){

    //First need to find node before deleteThis..


    //if node to be deleted is the head node..
    if (deleteThis == head){

        head = deleteThis->getNextAddress();
        delete deleteThis;
        return 1;

    }else {// if node to be deleted is not the head node

        node<TYPE> *tempNodeBeforePtr = getNodeBefore(head, deleteThis);

        tempNodeBeforePtr->setNextAddress( deleteThis->getNextAddress() );
        delete deleteThis;
        return 1;
    }
    return 0;

}

template <typename TYPE>
int deleteItem(node<TYPE>* &head, TYPE value){

    //First need to find node before deleteThis..

    node<TYPE>* nodeToDelete = searchNode(head, value);
    if (nodeToDelete->getCurrentAddress() == NULL){
        return 0; //Delete operation failed
    }else{
        deleteNode(head, nodeToDelete);
        return 1;
    }
    return 0;
}

template <typename TYPE>
node<TYPE>* getNodeBefore(node<TYPE>* head, node<TYPE>* beforeThis){

    node<TYPE> *tempNodeBeforePtr;
    tempNodeBeforePtr = head;
    while ( tempNodeBeforePtr->getNextAddress() != beforeThis->getCurrentAddress()){

        tempNodeBeforePtr = tempNodeBeforePtr->getNextAddress();
    }
    return tempNodeBeforePtr;

}

template <typename TYPE>
int printList(node<TYPE>* head, string delimiter){

    node<TYPE>* tempNodePtr = head;

    while (tempNodePtr != NULL){
        cout << tempNodePtr->getCurrentValue() << delimiter;
        tempNodePtr = tempNodePtr->getNextAddress();
    }
}




#endif // KELVINSL_LIST_H
