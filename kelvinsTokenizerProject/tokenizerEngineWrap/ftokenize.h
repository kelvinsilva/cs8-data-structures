#ifndef FTOKENIZE_H
#define FTOKENIZE_H
#include <iostream>
#include <fstream>
#include "stokenize.h"
#include "kelvinsLists\Frequency.h"
#include "tokenizerEngine\tokenEngine.h"
#include "tokenizerEngineWrap\token.h"
#include <stdexcept>

class FTokenize
{
private:

    //ftokenize extracts a block from text file and assigns to stokenizer class
    //stokenizer class retrieves tokens and passes tokens back to ftokenizer and up to caller
    //once stokenizer finishes tokenizing all data in block, then ftokenizer class reads in another block

       ifstream fileStream;

       STokenize stringTokenizer; //pass block to string tokenizer
       string sTokenizeBlock;

       int currentPosition;
       bool moreBlocks;
       bool moreTokensInBlock;

       bool getNewBlock();
       bool moreBlocksStatus();

public:

    //128 most efficient max block size.
    //later add functionality to change this
    const int maxBlock = 128;
    FTokenize(char* fname);
    //extract next token is the main api function to be used with tokenizer engine
    Token extractNextToken();

    bool moreTokensStatus();

    int getPos();
    int getBlockPos();
};

#endif // FTOKENIZE_H
