#ifndef STOKENIZE_H
#define STOKENIZE_H

#include <iostream>
#include <string>
#include "kelvinsLists\list.h"
#include "token.h"
#include "tokenizerEngine\tokenEngine.h"
#include "kelvinsLists\list.h"
#include "kelvinsLists/kelvinsPair.h"
#include <vector>

using namespace std;


class STokenize
{
private:

    //this vector contains all the types of tokens that we are testing for
    //user can add token type lists, or use setupDefaultFunction
    //Remember, when adding a custom list to test for, you need to assign one of the following enums that are defined in token.h
    vector<kelvinsPair<TOKENTYPE, char* > > tokenMatchList;

    //tokenizeThis is the block retrieved from ftokenizer.
    string tokenizeThis;
    char* charBlock;    //to maintain functionality with tokenizer engine api, this pointer is continually used to convert string to char block
    int currentPosition;
    char tokenStore[1024]{}; //store token in this block... max token size is 1024, but later functionality will
    TOKENTYPE currentType;
    list<Token> tokenList;
    bool moreTok;
    bool tkEn_GetNextToken(char block[], char token[], int &pos, TOKENTYPE &type);
    Token currentTok;

public:

    STokenize();
    STokenize(const string s);
    ~STokenize();
    friend STokenize& operator >> (STokenize& t, string& token);
    int getTokenType(char ch);
    string getThisToken(string charSet);
    string nextString();
    Token getNextToken();
    Token peekToken();
    void setTokenBlock(string tk);

    void setupDefaultTokenTest();

    bool fail();
    bool more();
    int pos();
};

#endif // STOKENIZE_H


