#include "token.h"

Token::Token()
{
    token = "";
    type = NO_TYPE;
}

Token::Token(string s, TOKENTYPE type){

    token = s;
    this->type = type;
}

Token::Token(char ch, TOKENTYPE type){

    token.push_back(ch);
    this->type = type;
}

TOKENTYPE& Token::getType(){

    return type;
}

string& Token::getStringReference(){

    return token;
}

string Token::getString(){

    return token;
}

ostream& operator<< (ostream& outs, Token& t){

    outs << t.token << " [TYPE = ";
    switch(t.type){
        case WHITE_SPACE:
            outs << "WHITESPACE";
            break;
        case END_OF_LINE:
            outs << "END OF LINE";
            break;
        case ALPHABETIC:
            outs << "ALPHABETIC";
            break;
        case NUMERICAL:
            outs << "NUMERICAL";
            break;
        case OPERATORS:
            outs << "OPERATORS";
            break;
        case PUNCTUATION:
            outs << "PUNCTUATION";
            break;
        case MATCHED_PAIRS:
            outs << "MATCHED PAIRS";
            break;
        case NO_TYPE:
            outs << "NO TYPE";
            break;
    }
    outs << "]";

    return outs;
}

bool Token::operator<(const Token& rhs) const{
    return (type == rhs.type) && (token < rhs.token);
}

bool Token::operator>(const Token& rhs) const{
    return (type == rhs.type) && (token > rhs.token);
}

bool Token::operator>= (const Token& rhs) const{
     return (type == rhs.type) && (token >= rhs.token);
}

bool Token::operator<= (const Token& rhs) const{
     return (type == rhs.type) && (token <= rhs.token);
}

bool Token::operator== (const Token& rhs) const{
    return (type == rhs.type) && (token == rhs.token);
}

void Token::setString(string str){

    token = str;
}
