#ifndef TABLE1_H
#define TABLE1_H

#include <cstdlib> //for size_t

using namespace std;

template <typename RecordType>
class table {
public:

    static const size_t CAPACITY = 9771;
    table();

    void insert(const RecordType& entry);
    void remove(int key);

    bool is_present(int key) const;
    void find(int key, bool& found, RecordType& result) const;

    size_t size() const { return used; }

private:

    static const int NEVER_USED = -1;
    static const int PREVIOUSLY_USED = -2;

    RecordType data[CAPACITY];
    size_t used;

    size_t hash(int key) const;
    size_t hash2(int key) const;
    size_t next_index(size_t index, int key) const;
    void find_index(int key, bool& found, size_t& index) const;
    bool never_used(size_t index) const;
    bool is_vacant(size_t index) const;




};

template <typename RecordType>
table <RecordType>::table(){

    size_t i;
    used = 0;
    for (i = 0; i < CAPACITY; ++i){

        data[i].key = NEVER_USED;
    }
}

template <typename RecordType>
void table<RecordType>::insert(const RecordType &entry){

    bool already_present;
    size_t index;

    //set index so that data[index] is the spot to place the new entry, index passed by reference,
    //areadypresent passed by reference, will be changed as well
    find_index(entry.key, already_present, index);

    if (!already_present){

        //this.size
        index = hash(entry.key);

        while(!is_vacant(index)){

            index = next_index(index, entry.key);
        }
        ++used;
    }

    data[index] = entry;
}

template <typename RecordType>
void table<RecordType>::remove(int key){

    bool found;
    size_t index;

    find_index(key, found, index);
    if (found){

        data[index].key = PREVIOUSLY_USED;
        --used;
    }
}

template <typename RecordType>
bool table<RecordType>::is_present(int key) const{

    bool found;
    size_t index;

    find_index(key, found, index);
    return found;
}

template <typename RecordType>
void table<RecordType>::find(int key, bool& found, RecordType& result) const{

    size_t index;

    find_index(key, found, index);
    if (found){

        result = data[index];
    }
}

template <typename RecordType>
inline size_t table<RecordType>::hash(int key) const{

    return (key % CAPACITY);
}

template <typename RecordType>
inline size_t table<RecordType>::hash2(int key) const{

    return 1 + (key % (CAPACITY - 2));
}

template <typename RecordType>
inline size_t table<RecordType>::next_index(size_t index, int key) const{

    return ((index + hash2(key)) % CAPACITY);
}

template <typename RecordType>
void table<RecordType>::find_index(int key, bool& found, size_t& i) const{

    size_t count;
    count = 0;
    i = hash(key);

    while ((count < CAPACITY) && (data[i].key != NEVER_USED) && (data[i].key != key)){

        ++count;
        i = next_index(i, key);
    }
    found = (data[i].key == key);
}

template <typename RecordType>
inline bool table<RecordType>::never_used(size_t index) const{

    return (data[index].key == NEVER_USED);
}

template <typename RecordType>
inline bool table<RecordType>::is_vacant(size_t index) const{

    return (data[index].key == NEVER_USED) || (data[index].key == PREVIOUSLY_USED);
}
#endif // TABLE1_H
