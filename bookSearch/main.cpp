#include <cctype>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
#include "table1.h"

using namespace std;

//book data structure
struct book
{
    int ISBN;
    string title;
    string author;
};

//In each lookup table we have a key, which contains an integer from our hash function (title, author or isbn), and position in library
struct findKey{

    int key;
    int position;
};

int hashISBN(int ISBN, int sizeMAX){

    return (ISBN * 133) % sizeMAX;
}

int hashTITLE(string TITLE, int sizeMAX){

    int ct = 0;
    for (int i = 0; i < TITLE.size(); i++){
        ct = ct + (int)TITLE[i];
    }

    return (ct % sizeMAX);
}

int hashAUTH(string AUTHOR, int sizeMAX){

    int ct = 0;
    for (int i = 0; i < AUTHOR.size(); i++){
        ct = ct + (int)AUTHOR[i];
    }

    return (ct % sizeMAX);
}

string generateRandomString(){

    char lookUpChar [] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
    char lookUpNum [] = "0123456789";

    string randStr;
    randStr.push_back( lookUpChar[ rand()%23 ] );
    randStr.push_back( lookUpChar[ rand()%23 ] );
    randStr.push_back( lookUpNum[ rand()%10 ] );
    randStr.push_back( lookUpNum[ rand()%10 ] );
    randStr.push_back( lookUpNum[ rand()%10 ] );

    return randStr;
}

const int librarySize = 200;


int main( )
{
    srand(time(NULL));

    //three methods of looking up data
    table<findKey>lookupISBN;
    table<findKey>lookupAUTHOR;
    table<findKey>lookupTITLE;


    book library[librarySize];
    //since I dont have a database to populate a library array
    //I just make author, isbn, and title randomly generated string and integers.
    for (int i = 0; i < librarySize; i++ ){
        book tempBook;
        tempBook.ISBN = rand()%2000 + 1;
        tempBook.author = generateRandomString();
        tempBook.title = generateRandomString();
        cout << "\nISBN: " << tempBook.ISBN << " index: "<< i << " author: " << tempBook.author << " title: " << tempBook.title;
        library[i] = tempBook;
    }

    //populate ISBN lookup table
    for (int i = 0; i < librarySize; i++){

        findKey tempKey;
        tempKey.position = i;
        tempKey.key = hashISBN(library[i].ISBN, lookupISBN.CAPACITY);
        lookupISBN.insert(tempKey);
    }

    //populate AUTHOR lookup table
    for (int i = 0; i < librarySize; i++){

        findKey tempKey;
        tempKey.position = i;
        tempKey.key = hashAUTH(library[i].author, lookupAUTHOR.CAPACITY);
        lookupAUTHOR.insert(tempKey);
    }

    //populate TITLE lookup table
    for (int i = 0; i < librarySize; i++){

        findKey tempKey;
        tempKey.position = i;
        tempKey.key = hashTITLE(library[i].title, lookupTITLE.CAPACITY);
        lookupTITLE.insert(tempKey);
    }

    //Begin user interaction, prompt for lookup requests.
    bool quit = false;
    while (!quit){
        int criteria = 0;

        cout << "\nChoose search criteria:\n";
        cout << "1. Search by ISBN\n";
        cout << "2. Search by Author\n";
        cout << "3. Search by Title\n";
        cout << "4. Quit library\n";
        cin >> criteria;

        book tempBook;
        findKey tempKey;
        bool found;

        switch (criteria){

            case 1:{ //ISBN
                int tempISBN = 0;
                cout << "\nType isbn here: ";
                cin >> tempISBN;
                lookupISBN.find(hashISBN(tempISBN, lookupISBN.CAPACITY), found, tempKey) ;

                if (!found){
                    cout << "\nThis ISBN does not exist\n";
                }else {
                    cout << "\nWe have found the book, at library index: " << tempKey.position;
                    tempBook = library[tempKey.position];
                    cout << "\nHere is the book:\n";
                    cout << "ISBN: " <<tempBook.ISBN << endl;
                    cout << "Author: " <<tempBook.author << endl;
                    cout << "Title: " <<tempBook.title << endl;
                }
            }
            break;

            case 2:{ //AUTHOR
                string tempAUTH = "";
                cout << "\nType author here: ";
                cin >> tempAUTH;
                lookupAUTHOR.find(hashAUTH(tempAUTH, lookupAUTHOR.CAPACITY), found, tempKey) ;

                if (!found){
                    cout << "\nThis AUTHOR does not exist\n";
                }else {
                    cout << "\nWe have found the book, at library index: " << tempKey.position;
                    tempBook = library[tempKey.position];
                    cout << "\nHere is the book:\n";
                    cout << "ISBN: " <<tempBook.ISBN << endl;
                    cout << "Author: " <<tempBook.author << endl;
                    cout << "Title: " <<tempBook.title << endl;
                }
            }
            break;

            case 3:{ //TITLE
                string tempTITLE = "";
                cout << "\nType title here: ";
                cin >> tempTITLE;
                lookupTITLE.find(hashTITLE(tempTITLE, lookupTITLE.CAPACITY), found, tempKey) ;

                if (!found){
                    cout << "\nThis Title does not exist\n";
                }else {
                    cout << "\nWe have found the book, at library index: " << tempKey.position;
                    tempBook = library[tempKey.position];
                    cout << "\nHere is the book:\n";
                    cout << "ISBN: " <<tempBook.ISBN << endl;
                    cout << "Author: " <<tempBook.author << endl;
                    cout << "Title: " <<tempBook.title << endl;
                }
            }
            break;

            default:
            case 4:{
                quit = true;
            }
        }
    }
    cout << "\nThank you for using the library.\n";

    return 0;
}
