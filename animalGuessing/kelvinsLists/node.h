//////////////////

#ifndef _NODE_CLASS
#define _NODE_CLASS

#include <iostream>

using namespace std;
//Node class for linked lists implementations.
template <typename TYPE>
class node {

    private:
        node *nextAddress;
        node *previousAddress;
        TYPE value;

    public:
        node();
        node(TYPE value);

        node* getCurrentAddress();
        node* getNextAddress();
        node* getPreviousAddress();
        TYPE getCurrentValue();
        TYPE& getCurrentValueReference();

        void setCurrentAddress(node* nodePointer);
        void setNextAddress(node* nodePointer);
        void setPreviousAddress(node* nodePointer);
        void setCurrentValue(TYPE val);
};

template <typename TYPE>
TYPE& node<TYPE>::getCurrentValueReference(){
    return value;
}


template <typename TYPE>
node<TYPE>::node(){

    nextAddress = NULL;
    previousAddress = NULL;
}

template <typename TYPE>
node<TYPE>::node(TYPE val){
    value = val;
    nextAddress = NULL;
    previousAddress = NULL;

}

template <typename TYPE>
void node<TYPE>::setNextAddress(node* nodePointer){

    nextAddress = nodePointer;
}

template <typename TYPE>
void node<TYPE>::setPreviousAddress(node* nodePointer){

    previousAddress = nodePointer;
}

template <typename TYPE>
void node<TYPE>::setCurrentValue(TYPE val){

    value = val;
}

template <typename TYPE>
node<TYPE>* node<TYPE>::getCurrentAddress(){

    return this;
}

template <typename TYPE>
node<TYPE>* node<TYPE>::getNextAddress(){

    return nextAddress;
}

template <typename TYPE>
node<TYPE>* node<TYPE>::getPreviousAddress(){

    return previousAddress;
}

template <typename TYPE>
TYPE node<TYPE>::getCurrentValue(){

    return value;
}


#endif // _NODE_CLASS
