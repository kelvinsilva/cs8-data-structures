#ifndef QUEUE_H
#define QUEUE_H

//Work In Progress

#include <iostream>
#include "node.h"

using namespace std;

template <typename TYPE>
class queue{


private:

    queue();

    node<TYPE>* listHead;

    int insertHead( node<TYPE>* &head, TYPE item);
    int deleteNode( node<TYPE>* &head, node<TYPE> *deleteThis);

    int count;


public:

    int front();
    int back();

    TYPE top();

    template <typename TYPE>
    int insertHead( node<TYPE>* &head, TYPE item);

    template <typename TYPE>
    int insertAfter( node<TYPE>* head, node<TYPE> *afterThis, TYPE item);

    template <typename TYPE>
    int insertBefore(node<TYPE>* head, node<TYPE>* beforeThis, TYPE item);

    template <typename TYPE>
    int insertSorted(node<TYPE>* &head, TYPE item);

    template <typename TYPE>
    int deleteNode( node<TYPE>* &head, node<TYPE> *deleteThis);

    template <typename TYPE>
    int deleteItem(node<TYPE>* &head, TYPE value);

    template <typename TYPE>
    node<TYPE>* searchNode(node<TYPE>* head, TYPE item);

    template <typename TYPE>
    int printList(node<TYPE>* head);

    template <typename TYPE>
    node<TYPE>* getNodeBefore(node<TYPE>* head, node<TYPE>* beforeThis);

};

#endif // QUEUE_H
