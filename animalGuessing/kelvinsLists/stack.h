#ifndef STACK_H
#define STACK_H

#include <iostream>
#include "node.h"

using namespace std;

template <typename TYPE>
class stack{


private:


    node<TYPE>* nodeHead;

    int insertHead( node<TYPE>* &head, TYPE item);
    int deleteNode( node<TYPE>* &head, node<TYPE> *deleteThis);

    int count;




public:

    stack();
    int push(TYPE item);
    int pop();
    TYPE top();
    int printStack();
    int getCount();

};



template <typename TYPE>
stack<TYPE>::stack(){

    nodeHead = NULL;
    count = 0;
}

template <typename TYPE>
int stack<TYPE>::push(TYPE item){

    insertHead(nodeHead, item);
    ++count;
}

template <typename TYPE>
int stack<TYPE>::getCount(){

    return count;
}

template <typename TYPE>
int stack<TYPE>::pop(){

    deleteNode(nodeHead, nodeHead);
    --count;
}

template <typename TYPE>
TYPE stack<TYPE>::top(){

    return nodeHead->getCurrentValue();

}

template <typename TYPE>
int stack<TYPE>::insertHead( node<TYPE>* &head, TYPE item){

    node<TYPE> *tempNodePtr = new node<TYPE>(item);
    tempNodePtr->setNextAddress(head);
    head = tempNodePtr;

    return 1;
}


template <typename TYPE>
int stack<TYPE>::deleteNode( node<TYPE>* &head, node<TYPE> *deleteThis){

    //First need to find node before deleteThis..


    //if node to be deleted is the head node..
    if (deleteThis == head){

        head = deleteThis->getNextAddress();
        delete deleteThis;
        return 1;

    }else {// if node to be deleted is not the head node

        node<TYPE> *tempNodeBeforePtr;
        tempNodeBeforePtr = head;
        while ( tempNodeBeforePtr->getCurrentAddress() != deleteThis->getCurrentAddress() && tempNodeBeforePtr->getCurrentAddress() != head){

            tempNodeBeforePtr = tempNodeBeforePtr->getNextAddress();
        }

        tempNodeBeforePtr->setNextAddress( deleteThis->getNextAddress() );
        delete deleteThis;
        return 1;
    }
    return 0;

}
template <typename TYPE>
int stack<TYPE>::printStack(){

    node<TYPE>* tempNodePtr = nodeHead;

    while (tempNodePtr != NULL){
        cout << tempNodePtr->getCurrentValue() << " <- ";
        tempNodePtr = tempNodePtr->getNextAddress();
    }

}


#endif // STACK_H
