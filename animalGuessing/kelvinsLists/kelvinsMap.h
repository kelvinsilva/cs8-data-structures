#ifndef KELVINSMAP_H
#define KELVINSMAP_H
#include "kelvinsL_List.h"
#include "kelvinsPair.h"

/*Usage:
 *kelvinsMap<TYPE, TYPE1> myMap; //instantiation
 *myMap["abc"] = 40; //add an item
 *cout << myMap["abc"]; //display item
 *use just like arrays
 */



template <typename TYPE_LEFT, typename TYPE_RIGHT>
class kelvinsMap{

private:

    node<kelvinsPair<TYPE_LEFT, TYPE_RIGHT> >* listHead;


public:

    //void insertItem(TYPE item);
    kelvinsMap();

    const TYPE_RIGHT operator[] (TYPE_LEFT key) const;
    TYPE_RIGHT& operator[](TYPE_LEFT key);

    void printMap();
    void insertMap(TYPE_LEFT key, TYPE_RIGHT value);
    void deletePair(TYPE_LEFT key);

    int getMapSize();

};


template <typename TYPE_LEFT, typename TYPE_RIGHT>
int kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::getMapSize(){

    return getCount(listHead);
}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
void kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::insertMap(TYPE_LEFT key, TYPE_RIGHT value){

    kelvinsPair<TYPE_LEFT, TYPE_RIGHT> tempPair;
    tempPair.left = key;
    tempPair.right = value;

    insertHead(listHead, tempPair );
}
template <typename TYPE_LEFT, typename TYPE_RIGHT>
void kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::deletePair(TYPE_LEFT key){

    kelvinsPair<TYPE_LEFT, TYPE_RIGHT> tempPair;
    tempPair.left = key;
    node<kelvinsPair<TYPE_LEFT, TYPE_RIGHT> >* tempNode = searchNode(listHead, tempPair);
    deleteNode(listHead, tempNode);

}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
void kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::printMap(){

    printList(listHead, string("\n") );

}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::kelvinsMap(){

    listHead = NULL;
}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
const TYPE_RIGHT kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::operator[](TYPE_LEFT key) const{

    kelvinsPair<TYPE_LEFT, TYPE_RIGHT> tempPair;
    tempPair.left = key;
    node<pair<TYPE_LEFT, TYPE_RIGHT> >* temp = searchNode(listHead, tempPair);

    return temp->getCurrentValue().right;

}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
TYPE_RIGHT& kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::operator[](TYPE_LEFT key){

    kelvinsPair<TYPE_LEFT, TYPE_RIGHT> tempPair;
    tempPair.left = key;
    node<kelvinsPair<TYPE_LEFT, TYPE_RIGHT> >* temp = searchNode(listHead, tempPair);

    if (temp == NULL){
        insertUniqueSorted(listHead, tempPair);
        node<kelvinsPair<TYPE_LEFT, TYPE_RIGHT> >* tmp = searchNode(listHead, tempPair);
        return tmp->getCurrentValueReference().right;
    }else {
        return temp->getCurrentValueReference().right;
    }
}

#endif // KELVINSMAP_H
