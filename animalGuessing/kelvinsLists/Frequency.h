#ifndef FREQUENCY_H
#define FREQUENCY_H
#include "node.h"
#include "kelvinsPair.h"


template <typename TYPENAME>
class frequencyList{

private:

    node<kelvinsPair<TYPENAME, int> >* listHead;

public:

    frequencyList();

    const int operator[](TYPENAME key) const;

    int& operator[](TYPENAME key);
    void insert(TYPENAME key);

    void printFrequency();
    void deleteFrequency(TYPENAME key);
};

template <typename TYPENAME>
frequencyList<TYPENAME>::frequencyList(){
    listHead = NULL;
}

template <typename TYPENAME>
void frequencyList<TYPENAME>::insert(TYPENAME key){
    kelvinsPair<TYPENAME, int> tempPair;
    tempPair.left = key;
    tempPair.right = 1;
    insertUniqueSorted(listHead, tempPair, ascendingOrder<kelvinsPair<TYPENAME, int> >(), addMe<kelvinsPair<TYPENAME, int> >());
}

template <typename TYPENAME>
const int frequencyList<TYPENAME>::operator[](TYPENAME key) const{
    kelvinsPair<TYPENAME, int> tempPair;
    tempPair.left = key;
    node<pair<TYPENAME, int> >* temp = searchNode(listHead, tempPair);

    return temp->getCurrentValue().right;
}

template <typename TYPENAME>
int& frequencyList<TYPENAME>::operator[](TYPENAME key){
    kelvinsPair<TYPENAME, int> tempPair;
    tempPair.left = key;
    tempPair.right = 1;
    node<kelvinsPair<TYPENAME, int> >* temp = searchNode(listHead, tempPair);

    if (temp == NULL){
        insertUniqueSorted(listHead, tempPair, ascendingOrder<kelvinsPair<TYPENAME, int> >(), addMe<kelvinsPair<TYPENAME, int> >());
        node<kelvinsPair<TYPENAME, int> >* tmp = searchNode(listHead, tempPair);
        return tmp->getCurrentValueReference().right;
    }else {
        return temp->getCurrentValueReference().right;
    }
}

template <typename TYPENAME>
void frequencyList<TYPENAME>::deleteFrequency(TYPENAME key){

    kelvinsPair<TYPENAME, int> tempPair;
    tempPair.left = key;
    node<kelvinsPair<TYPENAME, int> >* tempNode = searchNode(listHead, tempPair);
    deleteNode(listHead, tempNode);

}

template <typename TYPENAME>\
void frequencyList<TYPENAME>::printFrequency(){

    printList(listHead, string("\n") );
}

#endif FREQUENCY_H_

