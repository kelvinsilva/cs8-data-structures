#include "ftokenize.h"

FTokenize::FTokenize(char* fname){
   fileStream.open(fname);
   string name = "";
   if (fileStream.is_open()){
       getNewBlock();
       moreTokensInBlock = true;
       stringTokenizer.setupDefaultTokenTest();
   }else {
       throw std::invalid_argument("file does not exist");
   }

}

bool FTokenize::getNewBlock(){

    char* myBlock = new char[maxBlock];
    moreBlocks = extract_block_file(fileStream, myBlock, maxBlock);
    sTokenizeBlock = myBlock;
    delete myBlock;
    stringTokenizer.setTokenBlock(sTokenizeBlock);
    return moreBlocks;
}

Token FTokenize::extractNextToken(){
    Token myTempTok = stringTokenizer.getNextToken();
    moreTokensInBlock = stringTokenizer.more();
    if (moreTokensInBlock == false && moreBlocks == true){
        getNewBlock();
        moreTokensInBlock = true;
    }


    return myTempTok;
}


bool FTokenize::moreTokensStatus(){
    return  moreTokensInBlock || moreBlocks;
}

bool FTokenize::moreBlocksStatus(){
    return moreBlocks;
}

int FTokenize::getPos(){


}

int FTokenize::getBlockPos(){


}
