#ifndef TOKEN_H
#define TOKEN_H
#include <iostream>
#include <string>
using namespace std;
enum TOKENTYPE {

    WHITE_SPACE = 3,
    END_OF_LINE = 4,
    ALPHABETIC = 1,
    NUMERICAL = 5,
    OPERATORS = 6,
    PUNCTUATION = 2,
    MATCHED_PAIRS = 8,
    NO_TYPE = 9
};

class Token
{

private:

    string token;
    TOKENTYPE type;

public:
    Token();
    Token(string s, TOKENTYPE type);
    Token(char ch, TOKENTYPE type);

    string& getStringReference();
    string getString();

    void setString(string str);

    friend ostream& operator<< (ostream& outs, Token& t);
    TOKENTYPE& getType();

    bool operator==(const Token& rhs) const;

    bool operator<(const Token& rhs) const;
    bool operator>(const Token& rhs) const;

    bool operator>= (const Token& rhs) const;
    bool operator<= (const Token& rhs) const;

    string& TokenString();

};

#endif // TOKEN_H
