#include <iostream>
#include <cstdlib>
#include <string>
#include <stdexcept>

#include "tokenizerEngineWrap/ftokenize.h"
#include "kelvinsBinTree.h"

#include "bintree.h"
#include "useful.h"

using namespace std;
using namespace main_savitch_10;

void ask_and_move(kelvinsBinTree<string>* current_ptr);

kelvinsBinTree<string> beginning_tree();

void instruct();

void learn(kelvinsBinTree<string>* leaf_ptr);

void play(kelvinsBinTree<string>* current_ptr);

//kelvin silva
//void printLevelOrder(binary_tree<string>* root_ptr);

kelvinsBinTree<string> load_kelvins_bin_tree_from_file(char* fileName){

    kelvinsBinTree<string> tempTree(100);
    vector<string>* tempContainer = tempTree.getTreeContainer();

    FTokenize tokenizer( fileName );
    Token tempTok = tokenizer.extractNextToken();

    int i = 0;
    string currentStr = "";
    while(tokenizer.moreTokensStatus()){

        if (tempTok.getType() != END_OF_LINE){

            currentStr += tempTok.getString();

        }else if (tempTok.getType() == END_OF_LINE){

            tempContainer->at(i) = currentStr;
            currentStr = "";
            tempTree.isPresent[i] = true;
            i++;
        }
        tempTok = tokenizer.extractNextToken();
    }
    tempTree.treeSize = i;
    tempTree.currentNodeIndex = 0;

    return tempTree;
}

void write_kelvins_bin_tree_to_file(string fileName, kelvinsBinTree<string>* tree){

    ofstream outStr(fileName.c_str());
    vector<string>* binCont = tree->getTreeContainer();
    int treeSz = tree->sizeTree();

    if (outStr.fail()){

        cout << " wrong file name ";
        return;
    }else {
        for (int i = 0; i < treeSz; i++){
            outStr << binCont->at(i) << "\n";
        }
    }
    outStr.close();
}

int main()
{
    //Load from File
    kelvinsBinTree<string> tempTree = load_kelvins_bin_tree_from_file("testFile.txt");
    tempTree.shiftToRoot();

    instruct();

    do{

        play(&tempTree);
        tempTree.shiftToRoot();

    }while(inquire("Shall we play again?"));

    cout << "Thank you ";

    //Write To File
    write_kelvins_bin_tree_to_file("testFile.txt", &tempTree);

    return 0;
}
https://kelvinsilva@bitbucket.org/kelvinsilva/unix-bash-scripting-cs18.git
void ask_and_move(kelvinsBinTree<string>* current_ptr){

    cout << current_ptr->retrieve();
    if (inquire (  "please answer")){

        current_ptr->shiftLeft();
    }else{

        current_ptr->shiftRight();
    }
}

kelvinsBinTree<string> beginning_tree(){

    kelvinsBinTree<string> tempTree(100);

    const string root_question("Are you a mammal?");
    const string left_question("Are you bigger than a cat?");
    const string right_question("Do you live underwater");
    const string animal1("kangaroo");
    const string animal2("mouse");
    const string animal3("trout");
    const string animal4("robin");

    tempTree.createFirstNode(root_question);

    tempTree.addLeft(left_question);
    tempTree.shiftLeft();
    tempTree.addLeft(animal1);
    tempTree.addRight(animal2);

    tempTree.shiftToRoot();

    tempTree.addRight(right_question);
    tempTree.shiftRight();
    tempTree.addLeft(animal3);
    tempTree.addRight(animal4);

    return tempTree;
}



void instruct( )
// Library facilities used: iostream
{
    cout << "Let's play!" << endl;
    cout << "You pretend to be an animal, and I try to guess what you are.\n";
}

void learn(kelvinsBinTree<string>* leaf_ptr)
// Library facilities used: bintree.h, iostream, string, useful.h
{
    string guess_animal;    // The animal that was just guessed
    string correct_animal;  // The animal that the user was thinking of
    string new_question;    // A question to distinguish the two animals

    // Set strings for the guessed animal, correct animal and a new question.
    guess_animal = leaf_ptr->retrieve();
    cout << "I give up. What are you? " << endl;
    getline(cin, correct_animal);
    cout << "Please type a yes/no question that will distinguish a" << endl;
    cout << correct_animal << " from a " << guess_animal << "." << endl;
    cout << "Your question: " << endl;
    getline(cin, new_question);

    // Put the new question in the current node, and add two new children.
    leaf_ptr->change(new_question);
    cout << "As a " << correct_animal << ", " << new_question << endl;
    if (inquire("Please answer"))
    {
        leaf_ptr->addLeft( correct_animal );
        leaf_ptr->addRight( guess_animal );
    }
    else
    {
        leaf_ptr->addLeft( guess_animal );
        leaf_ptr->addRight(correct_animal );
    }
}

void play(kelvinsBinTree<string>* current_ptr){

    cout << "Thinks of an animal, then press return key.";
    eat_line();

    while( current_ptr->hasLeftChild() && current_ptr->hasRightChild() ){

        ask_and_move(current_ptr);
    }

    cout << "My guess is " << current_ptr->retrieve();
    if ( !inquire(". Am I right?") ){

        learn(current_ptr);
    }else {

        cout << "I knew it all along!" << endl;
    }
}
