#include <iostream>
#include "kelvinsBinNode.h"
#include "kelvinsBinSearchTree.h"
using namespace std;

int main(){

//testing tree
    kelvinsBinSearchTree<int> myTree;
    myTree.insertItem(10);
    myTree.insertItem(5);
    myTree.insertItem(7);
    myTree.insertItem(14);
    myTree.insertItem(12);
    myTree.insertItem(18);
    myTree.insertItem(15);
    myTree.printLevelOrder(cout);
    cout << "\nDelete 10 from tree:\n";
    myTree.deleteItem(10);
    myTree.printLevelOrder(cout);

    return 0;
}
