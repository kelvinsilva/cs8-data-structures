#ifndef _KELVINS_BIN_NODE_H_
#define _KELVINS_BIN_NODE_H_

#include <iostream>

using namespace std;

template <typename TYPE>
class kelvinsBinNode{

private:

    TYPE currentValue;

    kelvinsBinNode* leftLeafNode;
    kelvinsBinNode* rightLeafNode;

public:

    kelvinsBinNode();
    kelvinsBinNode(TYPE it);
    //modification member funx
    TYPE& getData();
    kelvinsBinNode<TYPE>* &leftLeaf();
    kelvinsBinNode<TYPE>* &rightLeaf();
    void setData(const TYPE& newData);
    void setLeftLeaf(kelvinsBinNode<TYPE>* newLeaf);
    void setRightLeaf(kelvinsBinNode<TYPE>* newLeaf);

    //constant
    const TYPE& getData() const;
    const kelvinsBinNode<TYPE>* leftLeaf() const;
    const kelvinsBinNode<TYPE>* rightLeaf() const;
    bool isLeaf() const;

};

template <typename TYPE>
kelvinsBinNode<TYPE>::kelvinsBinNode(){

    leftLeafNode = NULL;
    rightLeafNode = NULL;
}

template <typename TYPE>
kelvinsBinNode<TYPE>::kelvinsBinNode(TYPE it){

    leftLeafNode = NULL;
    rightLeafNode = NULL;
    currentValue = it;
}

template <typename TYPE>
TYPE& kelvinsBinNode<TYPE>::getData(){

    return currentValue;
}

template <typename TYPE>
kelvinsBinNode<TYPE>*& kelvinsBinNode<TYPE>::leftLeaf(){

    return leftLeafNode;
}

template <typename TYPE>
kelvinsBinNode<TYPE>*& kelvinsBinNode<TYPE>::rightLeaf(){

    return rightLeafNode;
}

template <typename TYPE>
void kelvinsBinNode<TYPE>::setData(const TYPE& newData){

    currentValue = newData;
}

template <typename TYPE>
void kelvinsBinNode<TYPE>::setLeftLeaf(kelvinsBinNode<TYPE>* newLeaf){

    leftLeafNode = newLeaf;
}

template <typename TYPE>
void kelvinsBinNode<TYPE>::setRightLeaf(kelvinsBinNode<TYPE>* newLeaf){

    rightLeafNode = newLeaf;
}

template <typename TYPE>
const TYPE& kelvinsBinNode<TYPE>::getData() const{

    return currentValue;
}

template <typename TYPE>
const kelvinsBinNode<TYPE>* kelvinsBinNode<TYPE>::leftLeaf() const{

    return leftLeafNode;
}

template <typename TYPE>
const kelvinsBinNode<TYPE>* kelvinsBinNode<TYPE>::rightLeaf() const{

    return rightLeafNode;
}

template <typename TYPE>
bool kelvinsBinNode<TYPE>::isLeaf() const{

    return ((rightLeafNode == NULL) && (leftLeafNode == NULL));
}

#endif // _KELVINS_BIN_NODE_H_
