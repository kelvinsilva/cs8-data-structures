#ifndef _KELVINS_BIN_SEARCH_TREE_H_
#define _KELVINS_BIN_SEARCH_TREE_H_

#include "kelvinsBinNode.h"
#include <cstring>

template <typename TYPE>
class kelvinsBinSearchTree{

private:

    kelvinsBinNode<TYPE>* treeRoot;

    int treeSize;
    int currentLevel;

    void insertItemRecursive(TYPE item, kelvinsBinNode<TYPE>* binPtr);
    void deleteItemRecursive(TYPE item, kelvinsBinNode<TYPE>* binPtr);
    TYPE deleteMin(kelvinsBinNode<TYPE>* &binPtr);

    int height(kelvinsBinNode<TYPE>* tempNode);
    void printLevel(ostream& outs, kelvinsBinNode<TYPE>* tempNode, int level);

    kelvinsBinNode<TYPE>* searchBinNode (TYPE item, kelvinsBinNode<TYPE>* binPtr);

public:

    kelvinsBinSearchTree();

    kelvinsBinNode<TYPE>* searchItem (TYPE item);
    void deleteItem(TYPE item);
    void insertItem(TYPE item);
    void printLevelOrder(ostream& outs);

};

template <typename TYPE>
kelvinsBinSearchTree<TYPE>::kelvinsBinSearchTree(){

    treeSize = 0;
    treeRoot = NULL;
}

template <typename TYPE>
void kelvinsBinSearchTree<TYPE>::insertItemRecursive(TYPE item, kelvinsBinNode<TYPE>* binPtr){

    if (treeSize == 0){

        treeRoot = new kelvinsBinNode<TYPE>(item);
    }else {

        if (item < binPtr->getData()){

            if (binPtr->leftLeaf() == NULL){

                binPtr->setLeftLeaf( new kelvinsBinNode<TYPE>(item) );
                return;
            }
            insertItemRecursive(item, binPtr->leftLeaf());

        }else if (item >= binPtr->getData()) {

            if (binPtr->rightLeaf() == NULL){

                binPtr->setRightLeaf( new kelvinsBinNode<TYPE>(item) );
                return;
            }
            insertItemRecursive(item, binPtr->rightLeaf());
        }
    }
}

template <typename TYPE>
void kelvinsBinSearchTree<TYPE>::insertItem(TYPE item){

    insertItemRecursive(item, treeRoot);
    ++treeSize;
}

template <typename TYPE>
void kelvinsBinSearchTree<TYPE>::deleteItem(TYPE item){

    deleteItemRecursive(item, treeRoot);
    --treeSize;
}

template <typename TYPE>
void kelvinsBinSearchTree<TYPE>::deleteItemRecursive(TYPE item, kelvinsBinNode<TYPE>* binPtr){

    if (binPtr != NULL){

        if ( item < binPtr->getData()){

            deleteItemRecursive(item, binPtr->leftLeaf());
        }else if (item > binPtr->getData()){

            deleteItemRecursive(item, binPtr->rightLeaf());
        }else if (binPtr->isLeaf()){    //leftchild == NULL and rightChil == NULL

            kelvinsBinNode<TYPE>* tempPrevNode = treeRoot;

            //This while loop searches for the node before the node to become deleted
            while (tempPrevNode->leftLeaf() != binPtr && tempPrevNode->rightLeaf() != binPtr){

                if (binPtr->getData() < tempPrevNode->getData()){

                    tempPrevNode = tempPrevNode->leftLeaf();
                }else if (binPtr->getData() > tempPrevNode->getData()){

                    tempPrevNode = tempPrevNode->rightLeaf();
                }
            }
            if (tempPrevNode->leftLeaf() == binPtr){

                tempPrevNode->leftLeaf() = NULL;
            }else if (tempPrevNode->rightLeaf() == binPtr){

                tempPrevNode->rightLeaf() = NULL;
            }

            delete binPtr;
            binPtr = NULL;

            return;
        }else if (binPtr->leftLeaf() == NULL){

            kelvinsBinNode<TYPE>* temp = binPtr->rightLeaf();
            kelvinsBinNode<TYPE>* tempAddr = binPtr;
            delete binPtr;

            memmove(tempAddr, temp, sizeof(kelvinsBinNode<TYPE>));
            return;

        }else if (binPtr->rightLeaf() == NULL){

            kelvinsBinNode<TYPE>* temp = binPtr->leftLeaf();
            kelvinsBinNode<TYPE>* tempAddr = binPtr;

            delete binPtr;
            memmove(tempAddr, temp, sizeof(kelvinsBinNode<TYPE>));
            return;

        }else {

            binPtr->setData( deleteMin(binPtr->rightLeaf()) );
        }
    }
}

//Delete the lowest valued element of descedants of right child.
//Data structures and algorithms by alfred v aho, john e hopcroft, jeffrey d ullman
template <typename TYPE>
TYPE kelvinsBinSearchTree<TYPE>::deleteMin(kelvinsBinNode<TYPE>* &binPtr){

    if (binPtr->leftLeaf() == NULL){

        TYPE item = binPtr->getData();
        binPtr = binPtr->rightLeaf();

        return item;
    } else {

        deleteMin(binPtr->leftLeaf());
    }
}

template <typename TYPE>
kelvinsBinNode<TYPE>* kelvinsBinSearchTree<TYPE>::searchBinNode (TYPE item, kelvinsBinNode<TYPE>* binPtr){

    if (item < binPtr->getData()){

        return searchBinNode(item, binPtr->leftLeaf());
    }else if (item > binPtr->getData()){

        return searchBinNode(item, binPtr->rightLeaf());
    }
    if (binPtr->getData() == item){
        return binPtr;
    }
}

template <typename TYPE>
kelvinsBinNode<TYPE>* kelvinsBinSearchTree<TYPE>::searchItem (TYPE item){

    return searchBinNode(item, treeRoot);
}

//finds total height of tree
template <typename TYPE>
int kelvinsBinSearchTree<TYPE>::height(kelvinsBinNode<TYPE>* tempNode){

    if (tempNode == NULL){

        return 0;
    }else {

        int leftHeight = height(tempNode->leftLeaf());
        int rightHeight = height(tempNode->rightLeaf());

        if (leftHeight > rightHeight){

            return (leftHeight + 1);
        }else {

            return (rightHeight + 1);
        }
    }
}

template <typename TYPE>
void kelvinsBinSearchTree<TYPE>::printLevel(ostream& outs, kelvinsBinNode<TYPE>* tempNode, int level){

    if (tempNode == NULL){
        cout << " * ";
        return;
    }
    if (level == 1){

        outs << tempNode->getData() << " ";
    }else if (level > 1){

        printLevel(cout, tempNode->leftLeaf(), level-1);
        printLevel(cout, tempNode->rightLeaf(), level-1);
    }
}

template <typename TYPE>
void kelvinsBinSearchTree<TYPE>::printLevelOrder(ostream& outs){

    int h = height(treeRoot);
    for(int i = 1; i <= h; i++){

        printLevel(outs, treeRoot, i);
        cout << "\n";
    }
}

#endif // _KELVINS_BIN_SEARCH_TREE_H_
