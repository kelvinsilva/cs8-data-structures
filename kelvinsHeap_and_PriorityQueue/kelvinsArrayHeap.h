#ifndef _KELVINS_ARRAY_HEAP_H
#define _KELVINS_ARRAY_HEAP_H

#include <vector>

using namespace std;

//this is a max heap. no choice of min or max. max by default
template <typename TYPE>
class kelvinsArrayHeap {

private:

    //usage of vector to allow variable array size.
    vector<TYPE> arrayHeap;

public:

    void insert(TYPE item);

    TYPE getTop();
    void removeTop();

    int getSize();

    void printArray();

    //takes contents of tempArray and replaces into arrayHeap, with heap property.
    void heapify(vector<TYPE>* tempArray);

};

template <typename TYPE>
int kelvinsArrayHeap<TYPE>::getSize(){

    return arrayHeap.size();
}

template <typename TYPE>
void kelvinsArrayHeap<TYPE>::insert(TYPE item){

    if ( arrayHeap.size() == 0){    //edge case if there is nothing in vector, put at 0th index

        arrayHeap.push_back(item);
    }else {

        arrayHeap.push_back(item);
        int n = arrayHeap.size() - 1; //N of 2N + 1 or 2N + 2 or N + 1 / 2
        int parent = (n - 1) / 2;

        //while parent is less than child.
        while (arrayHeap[parent] < arrayHeap[n]){

            //swap the two nodes
            swap (arrayHeap[parent], arrayHeap[n]);
            //reflect changes from swapping...
            n = parent; //Child is now parent
            parent = (parent - 1) / 2;  //Parent is pointed to parent above it.
        }
    }
}

template <typename TYPE>
void kelvinsArrayHeap<TYPE>::printArray(){
    //traverse and print contents of vector.
    typename vector<TYPE>::iterator it;
    for (it = arrayHeap.begin(); it != arrayHeap.end(); ++it){

        cout << " " << *it << " ";
    }
}

template <typename TYPE>
TYPE kelvinsArrayHeap<TYPE>::getTop(){
    //max value always at 0th index
    return arrayHeap[0];
}

template <typename TYPE>
void kelvinsArrayHeap<TYPE>::removeTop(){

    arrayHeap[0] = arrayHeap[arrayHeap.size() - 1];
    arrayHeap.pop_back();

    int root = 0;
    //if there are right or left children, then assign correct index, if there are no children, do not change index
    //since tree in vector is always complete if there is a right child there will always be a left child.
    //but if there is no right child, then left child can still exist, thus if there is no right child, then we set right child index to equal left child.
    int leftChildIndex = (arrayHeap.size()-1 >= (2 * root) + 1) ? ((2 * root) + 1) : root;
    int rightChildIndex = (arrayHeap.size()-1 >= (2 * root) + 2) ? ((2 * root) + 2) : leftChildIndex;

    TYPE leftChildValue;
    TYPE rightChildValue;

    //follow standard binary heap deletion algorithm
    while ( arrayHeap[root] < arrayHeap[leftChildIndex] || arrayHeap[root] < arrayHeap[rightChildIndex] ){

        if (leftChildIndex){

            leftChildValue = arrayHeap[leftChildIndex];
        }
        if (rightChildIndex){

            rightChildValue = arrayHeap[rightChildIndex];
        }

        if (leftChildValue >= rightChildValue){

            swap(arrayHeap[root], arrayHeap[leftChildIndex]);
            root = leftChildIndex; // move to left child index
        }else if (leftChildValue < rightChildValue){

            swap(arrayHeap[root], arrayHeap[rightChildIndex]);
            root = rightChildIndex;  //move to right child index
        }

        leftChildIndex = (arrayHeap.size() > (2 * root) + 1) ? ((2 * root) + 1) : root;
        rightChildIndex = (arrayHeap.size() > (2 * root) + 2) ? ((2 * root) + 2) : leftChildIndex;
    }
}

template <typename TYPE>
void kelvinsArrayHeap<TYPE>::heapify(vector<TYPE>* tempArray){

    //to heapify random vector, just clear current vector, and reinsert all items
    //in temparray into object
    arrayHeap.clear();
    typename vector<TYPE>::iterator it;

    for (it = tempArray->begin(); it != tempArray->end(); ++it){

        this->insert(*it);
    }
}

#endif // KELVINSARRAYHEAP_H
