#ifndef KELVINS_NEW_PRIORITY_QUEUE_H
#define KELVINS_NEW_PRIORITY_QUEUE_H

#include "kelvinsArrayHeap.h"

//classic wrapper for priority queue
template <typename TYPE>
class kelvinsNewPriorityQueue{

private:

    kelvinsArrayHeap<TYPE> heapTree;

public:

    void push(const TYPE& item);
    void pop();
    TYPE top();

    int size();
    bool empty();
};

template <typename TYPE>
void kelvinsNewPriorityQueue<TYPE>::push(const TYPE& item){

    heapTree.insert(item);
}

template <typename TYPE>
void kelvinsNewPriorityQueue<TYPE>::pop(){

    heapTree.removeTop();
}

template <typename TYPE>
TYPE kelvinsNewPriorityQueue<TYPE>::top(){

    return heapTree.getTop();
}

template <typename TYPE>
int kelvinsNewPriorityQueue<TYPE>::size(){

    return heapTree.getSize();
}

template <typename TYPE>
bool kelvinsNewPriorityQueue<TYPE>::empty(){

    return (heapTree.getSize() == 0);
}

#endif // KELVINSNEWPRIORITYQUEUE_H
