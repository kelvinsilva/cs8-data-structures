#include <iostream>
#include <cstdlib>
#include <ctime>

#include "kelvinsArrayHeap.h"
#include "kelvinsNewPriorityQueue.h"

using namespace std;

int main(){

    //Do some test insertions into heap class
    kelvinsArrayHeap<int> myHeap;
    myHeap.insert(11);
    myHeap.insert(5);
    myHeap.insert(2);
    myHeap.insert(8);
    myHeap.insert(3);
    myHeap.insert(4);
    myHeap.insert(2);
    myHeap.insert(-99999);
    myHeap.insert(1);
    myHeap.insert(22);
    myHeap.insert(99);
    myHeap.insert(3);

    myHeap.printArray();

    //remove each element from top. We should see numbers being printed from biggest to smallest.
    //every time a top is printed, the corresponding vector list is also printed with it
    int size = myHeap.getSize();
    for (int i = 0; i < size; i++){
        cout << endl << myHeap.getTop() << endl;
        myHeap.printArray();
        myHeap.removeTop();
    }

    //test heapification of vector. insert a vector with random numbers in
    //random order and put in side heap structure to heapify.

    vector<int> heapMe;
    heapMe.push_back(5);
    heapMe.push_back(9);
    heapMe.push_back(2);
    heapMe.push_back(98);

    cout << "\nheapMe vector heapified: ";
    myHeap.heapify(&heapMe);
    myHeap.printArray();

    //check that it is heapified.
    cout << "\nTop of heap: " << myHeap.getTop();

    myHeap.removeTop();
    cout << endl;
    myHeap.printArray();
    cout << "\nHeap Size: " << myHeap.getSize() << "\nNext largest value: " << myHeap.getTop();

    //test priority queue interface for binary heap. we will be pushing
    //many random items and popping many random items
    //we see that it works properly if we see decrementing numbers as we pop from queue

    cout << "\n\n";
    kelvinsNewPriorityQueue<unsigned long> priorities;
    priorities.push(10);
    priorities.push(9);
    priorities.push(99);
    priorities.push(3);

    cout << "\npushed 10, 9, 99, 3 into Priority queue: ";
    cout << "\nTop of queue: " << priorities.top();
    int s = priorities.size();
    for (int i = 0; i < s; i++){

        cout << "\nPriorities: " << priorities.top();
        priorities.pop();
    }

    //push many random numbers to queue and pop, if it works we will see decrementing number order being printed
    srand(time(NULL));
    for (int i = 0; i < 15; i++){

        priorities.push(rand());
    }
    int sz = priorities.size();
    for (int i = 0; i < sz; i++){
        cout << endl << priorities.top();
        priorities.pop();
    }
    return 0;
}

