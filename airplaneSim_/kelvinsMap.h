#ifndef _KELVINS_MAP_H_
#define _KELVINS_MAP_H_

#include "kelvinsNode.h"
#include "kelvinsPair.h"
#include "kelvinsListHelpers.h"


template <typename TYPE_LEFT, typename TYPE_RIGHT>
class kelvinsMap{

private:

    kelvinsNode<kelvinsPair<TYPE_LEFT, TYPE_RIGHT> >* listHead;


public:

    kelvinsMap();
    ~kelvinsMap();

    void insertMap(TYPE_LEFT key, TYPE_RIGHT value);

    void deletePair(TYPE_LEFT key);

    void printMap();

    const TYPE_RIGHT operator[] (TYPE_LEFT key) const;
    TYPE_RIGHT& operator[](TYPE_LEFT key);

};

template <typename TYPE_LEFT, typename TYPE_RIGHT>
kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::kelvinsMap(){

    listHead = NULL;
}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::~kelvinsMap(){

    deleteList(listHead);
}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
void kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::insertMap(TYPE_LEFT key, TYPE_RIGHT value){

    kelvinsPair<TYPE_LEFT, TYPE_RIGHT> tempPair;
    tempPair.left = key;
    tempPair.right = value;

    insertHead(listHead, tempPair );
}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
void kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::deletePair(TYPE_LEFT key){

    kelvinsPair<TYPE_LEFT, TYPE_RIGHT> tempPair;
    tempPair.left = key;
    kelvinsNode<kelvinsPair<TYPE_LEFT, TYPE_RIGHT> >* tempNode = searchNode(listHead, tempPair);
    deleteNode(listHead, tempNode);
}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
void kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::printMap(){

    printList(listHead, string("\n") );
}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
const TYPE_RIGHT kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::operator[](TYPE_LEFT key) const{

    kelvinsPair<TYPE_LEFT, TYPE_RIGHT> tempPair;
    tempPair.left = key;
    kelvinsNode<pair<TYPE_LEFT, TYPE_RIGHT> >* temp = searchNode(listHead, tempPair);

    return temp->getCurrentValue().right;
}

template <typename TYPE_LEFT, typename TYPE_RIGHT>
TYPE_RIGHT& kelvinsMap<TYPE_LEFT, TYPE_RIGHT>::operator[](TYPE_LEFT key){

    kelvinsPair<TYPE_LEFT, TYPE_RIGHT> tempPair;
    tempPair.left = key;
    kelvinsNode<kelvinsPair<TYPE_LEFT, TYPE_RIGHT> >* temp = searchNode(listHead, tempPair);

    if (temp == NULL){
        insertUniqueSorted(listHead, tempPair);
        kelvinsNode<kelvinsPair<TYPE_LEFT, TYPE_RIGHT> >* tmp = searchNode(listHead, tempPair);
        return tmp->getCurrentValueReference().right;
    }else {
        return temp->getCurrentValueReference().right;
    }
}

#endif // KELVINSMAP_H
