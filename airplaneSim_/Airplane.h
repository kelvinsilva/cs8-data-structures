#ifndef _AIRPLANE_H_
#define _AIRPLANME_H_

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

string generateRandomString();

//Airplane structure to hold various datas
struct Airplane{

    Airplane();
    Airplane(int crashSec, int takeSec, string name);

    int secondsTakesToLand;
    int secondsTakesToTakeoff;

    int secondsBeforeCrash;    //Amount of fuel left in plane.
    int secondsBeforeTakeoff;  //Departure time

    int landingProgress = 0;

    bool isLanding;
    bool isTakingoff;
    bool delayedDeparture;

    string planeId;


    friend ostream& operator<<(ostream &out, const Airplane& input);
};

#endif
