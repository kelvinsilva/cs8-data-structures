#include <iostream>
#include <ctime>
#include <windows.h>
#include <cstdlib>
#include <vector>
#include "kelvinsPriorityQueue.h"
#include "kelvinsList.h"
#include "Airplane.h"

using namespace std;

string generateRandomString();

//Probability class from text book.
class boolRandom{

private:

    double probability;

public:

    boolRandom(double p){

        probability = p;
    }
    bool queryProbability(){

        return (rand() < probability * RAND_MAX);
    }
};

//Airplane simulation function
int airplaneSimulation(int secondsBeforeCrashMax, int secondsBeforeTakeoffMax, int _secondsTakesToLand,
                         int _secondsTakesToTakeoff, double probabilityLand, double probabilityTakeoff, int timeTotalInSeconds);


int main(){

    int secondsBeforeCrashMax = 50;
    int secondsBeforeTakeoffMax = 100;
    int _secondsTakesToLand = 5;
    int _secondsTakesToTakeoff = 5;
    double probabilityLand = 0;

    double probabilityTakeoff = .3;
    int timeTotalInSeconds = 100;

    vector<int> arr(10);
    arr.push_back(2);
    cout << arr[9];
    return 0;
   // airplaneSimulation(secondsBeforeCrashMax, secondsBeforeTakeoffMax, _secondsTakesToLand,
                       //  _secondsTakesToTakeoff, probabilityLand, probabilityTakeoff, timeTotalInSeconds);
}

int airplaneSimulation(int secondsBeforeCrashMax, int secondsBeforeTakeoffMax, int _secondsTakesToLand,
                         int _secondsTakesToTakeoff, double probabilityLand, double probabilityTakeoff, int timeTotalInSeconds){

    srand(time(NULL));

    boolRandom landingGenerator(probabilityLand);
    boolRandom takeoffGenerator(probabilityTakeoff);

    //There is only one runway.
    //A runway can be either used for takeoff or landing, but not both.
    kelvinsPriorityQueue<Airplane, int, ascendingOrder<kelvinsPair<int, Airplane> > > planesWaitingToLand;
    kelvinsPriorityQueue<Airplane, int, ascendingOrder<kelvinsPair<int, Airplane> > > planesWaitingToTakeoff; //ascending order

    //Planes that are not a liability. Pleans from the priority queue end up in one of the three lists.
    kelvinsList<Airplane> planesDeparted;
    kelvinsList<Airplane> planesLanded;
    kelvinsList<Airplane> junkYard; //for crashed planes

    bool runwayInUse = false;

    int timePassed = 0;

    Airplane landingAirplane;   //A temporary variable to hold a currently landing airplane.
    //An airplane will be put in landing state, if there is another airplane with less fuel that is put into the queue while another airplane is landing, then the lower fuel airplane will crash


    while (timePassed < timeTotalInSeconds){

        cout << "\n*****************************NEW SECOND**************************************\n";
        cout << "Timepassed: " << timePassed << endl;

        if (landingGenerator.queryProbability()){   //If our probability gives us a landing then we prepare a structure to add to priorityQueue
            cout << "sup";
            Airplane tempAp;
            tempAp.secondsBeforeCrash = (rand() % secondsBeforeCrashMax) + 1;
            tempAp.secondsBeforeTakeoff = 0;
            tempAp.isLanding = false;
            tempAp.isTakingoff = false;
            tempAp.secondsTakesToLand = _secondsTakesToLand;    //function parameter
            tempAp.landingProgress = _secondsTakesToLand;
            tempAp.secondsTakesToTakeoff =  _secondsTakesToTakeoff;
            planesWaitingToLand.pushWithPriority(tempAp, tempAp.secondsBeforeCrash);
        }

        if (takeoffGenerator.queryProbability()){

            Airplane tempAp;
            tempAp.secondsBeforeCrash = 9999999;    //This parameter does not matter.
            tempAp.secondsBeforeTakeoff = (rand() % secondsBeforeTakeoffMax) + 1;
            tempAp.isLanding = false;
            tempAp.isTakingoff = true;
            tempAp.delayedDeparture = false;
            tempAp.secondsTakesToLand = _secondsTakesToLand;    //functions parameter
            tempAp.secondsTakesToTakeoff =  _secondsTakesToTakeoff;
            tempAp.landingProgress = _secondsTakesToLand;

            planesWaitingToTakeoff.pushWithPriority(tempAp, tempAp.secondsBeforeTakeoff);
        }

        for (int i = 0; i < planesWaitingToTakeoff.getPriorityQueueSize(); i++){
            //For every airplane in takeoff queue, we take off a second and if the seconds run out, then the plane is put into a delayed state,
            Airplane* tempAirplanePtr = planesWaitingToTakeoff.getAddressOfItemAtIndex(i);
            tempAirplanePtr->secondsBeforeTakeoff -= 1;
            if (tempAirplanePtr->secondsBeforeTakeoff < 0 && !tempAirplanePtr->delayedDeparture){
                //Planes in delayed state will take off only if the queue has no more airplanes in a takeoff state. IE they are the last to take off.
                tempAirplanePtr->delayedDeparture = true;
                tempAirplanePtr->secondsBeforeTakeoff = 0;
            }
        }

        for (int i = 0; i < planesWaitingToLand.getPriorityQueueSize(); i++){
            //For every plane trying to land, subtract one second to its fuel capacity.
            Airplane* tempAirplanePtr = planesWaitingToLand.getAddressOfItemAtIndex(i);
            tempAirplanePtr->secondsBeforeCrash -= 1;
        }

        if (runwayInUse == false && planesWaitingToLand.getPriorityQueueSize() > 0){

            landingAirplane = planesWaitingToLand.top();

            runwayInUse = true;
            landingAirplane.isLanding = true;
            planesWaitingToLand.pop();
        }
        if (runwayInUse == true && planesWaitingToLand.getPriorityQueueSize() > 0){
            //If a certain airplane is landing, but another airplane in the queue has run out of fuel, then that airplane crashes an dis put into the junkyard.
            Airplane tempAp = planesWaitingToLand.top();
            if (tempAp.secondsBeforeCrash <= 0){
                cout << "\n!-*AIRPLANE CRASHED*-!\n";
                junkYard.insertItem(tempAp);
                planesWaitingToLand.pop();
            }
        }

        if (landingAirplane.isLanding == true){
            //For the airplane in landing state, subtract 1 from landing progress.
            landingAirplane.landingProgress -= 1;
        }

        if (landingAirplane.landingProgress <= 0 && runwayInUse && landingAirplane.isLanding){
            //When landing progress is done, put airplane in the list, and reset the flags.
            planesLanded.insertItem(landingAirplane);
            runwayInUse = false;
            landingAirplane.isLanding = false;
        }


        //handle case of taking off airplanes
        if (runwayInUse == false && planesWaitingToLand.getPriorityQueueSize() <= 0 && planesWaitingToTakeoff.getPriorityQueueSize() > 0){ //if runway available and no planes waiting to land

            Airplane tempAp = planesWaitingToTakeoff.top();
            if (tempAp.secondsBeforeTakeoff <= 0){

                runwayInUse = true;
                planesDeparted.insertItem(tempAp);
                planesWaitingToTakeoff.pop();
            }
            runwayInUse = false;
        }

        cout << "\nplanes waiting to takeoff:\n";
        planesWaitingToTakeoff.printPriorityQueue();
        cout << "\nPlanes departed: ";
        planesDeparted.printListEx();
        cout << endl;

        cout << "\nPlanes waiting to land:\n";
        planesWaitingToLand.printPriorityQueue();
        cout << "\nPlanes landed:\n";
        planesLanded.printListEx();
        cout << "\nPlanes Crashed:\n";
        junkYard.printListEx();


        cout << "\n********************************************************************************\n";
        timePassed++;
        cout <<"Press enter for next Iteration: ";
        cin.get();
    }
    return 0;
}
