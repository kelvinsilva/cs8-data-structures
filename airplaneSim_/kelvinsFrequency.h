#ifndef _KELVINS_FREQUENCY_H
#define _KELVINS_FREQUENCY_H

#include <iostream>
#include "kelvinsNode.h"
#include "kelvinsPair.h"
#include "kelvinsListHelpers.h"


template <typename TYPENAME>
class kelvinsFrequency{

private:

    kelvinsNode<kelvinsPair<TYPENAME, int> >* listHead;

public:

    kelvinsFrequency();
    ~kelvinsFrequency();

    const int operator[](TYPENAME key) const;
    int& operator[](TYPENAME key);

    void insertFrequency(TYPENAME key);

    void deleteFrequency(TYPENAME key);

    void printFrequency();

};

template <typename TYPENAME>
kelvinsFrequency<TYPENAME>::~kelvinsFrequency(){

    deleteList(listHead);
}

template <typename TYPENAME>
kelvinsFrequency<TYPENAME>::kelvinsFrequency(){
    listHead = NULL;
}

template <typename TYPENAME>
void kelvinsFrequency<TYPENAME>::insertFrequency(TYPENAME key){
    kelvinsPair<TYPENAME, int> tempPair;
    tempPair.left = key;
    tempPair.right = 1;
    insertUniqueSorted(listHead, tempPair, ascendingOrder<kelvinsPair<TYPENAME, int> >(), addMe<kelvinsPair<TYPENAME, int> >());
}

template <typename TYPENAME>
const int kelvinsFrequency<TYPENAME>::operator[](TYPENAME key) const{
    kelvinsPair<TYPENAME, int> tempPair;
    tempPair.left = key;
    kelvinsNode<pair<TYPENAME, int> >* temp = searchNode(listHead, tempPair);

    return temp->getCurrentValue().right;
}

template <typename TYPENAME>
int& kelvinsFrequency<TYPENAME>::operator[](TYPENAME key){
    kelvinsPair<TYPENAME, int> tempPair;
    tempPair.left = key;
    tempPair.right = 1;
    kelvinsNode<kelvinsPair<TYPENAME, int> >* temp = searchNode(listHead, tempPair);

    if (temp == NULL){
        insertUniqueSorted(listHead, tempPair, ascendingOrder<kelvinsPair<TYPENAME, int> >(), addMe<kelvinsPair<TYPENAME, int> >());
        kelvinsNode<kelvinsPair<TYPENAME, int> >* tmp = searchNode(listHead, tempPair);
        return tmp->getCurrentValueReference().right;
    }else {
        return temp->getCurrentValueReference().right;
    }
}

template <typename TYPENAME>
void kelvinsFrequency<TYPENAME>::deleteFrequency(TYPENAME key){

    kelvinsPair<TYPENAME, int> tempPair;
    tempPair.left = key;
    kelvinsNode<kelvinsPair<TYPENAME, int> >* tempNode = searchNode(listHead, tempPair);
    deleteNode(listHead, tempNode);

}

template <typename TYPENAME>
void kelvinsFrequency<TYPENAME>::printFrequency(){

    printList(listHead, string("\n") );
}

#endif //_KELVINS_FREQUENCY_H_

