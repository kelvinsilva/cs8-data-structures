#include "Airplane.h"


Airplane::Airplane(){

    secondsBeforeCrash = 0;
    secondsBeforeTakeoff = 0;
    planeId = generateRandomString();  //generate random ID
    delayedDeparture = false;

}

Airplane::Airplane(int crashSec, int takeSec, string name){
    secondsBeforeCrash = crashSec;
    secondsBeforeTakeoff = takeSec;
    planeId = name;  //generate randomID

    isLanding = (secondsBeforeTakeoff == 0);
    isTakingoff = (secondsBeforeTakeoff > 0);
    delayedDeparture = false;

}

ostream& operator<<(ostream &out, const Airplane& input){

    if (input.isTakingoff){
        if (input.delayedDeparture){

            out << input.planeId << " -Flight Delayed- ";
        }else {

            out << input.planeId << " -Departing- " << " in " << input.secondsBeforeTakeoff << " seconds";
        }
    }else{

        out << input.planeId << " -Arriving- " << " in " << input.secondsBeforeCrash << " seconds";
    }
}

//Generates a random string for airplane ID name 2 letters and 3 numbers
string generateRandomString(){

    char lookUpChar [] = {"ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
    char lookUpNum [] = "0123456789";

    string randStr;
    randStr.push_back( lookUpChar[ rand()%23 ] );
    randStr.push_back( lookUpChar[ rand()%23 ] );
    randStr.push_back( lookUpNum[ rand()%10 ] );
    randStr.push_back( lookUpNum[ rand()%10 ] );
    randStr.push_back( lookUpNum[ rand()%10 ] );

    return randStr;
}
