#ifndef _KELVINS_SORTED_LIST_H_
#define _KELVINS_SORTED_LIST_H_

#include "kelvinsNode.h"
#include "kelvinsListHelpers.h";


template <typename TYPE>
class kelvinsSortedList{

private:

   kelvinsNode<TYPE>* listHead;

public:

   kelvinsSortedList();
   ~kelvinsSortedList();

   void insertItem(TYPE item);

   void deleteItem(TYPE item);
   void deleteItemAt(int position);

   int getSortedListSize();

   void reverseSortedList();
   void printSortedList();

    kelvinsNode<TYPE>* searchSortedList(TYPE item);

   template <typename T>
   friend ostream& operator<<(ostream &out, const kelvinsList<T> &lst);

   template <typename T>
   friend istream& operator>>(istream &in, kelvinsList<T> &lst);

};

template <typename TYPE>
kelvinsSortedList<TYPE>::kelvinsSortedList(){

    listHead = NULL;
}

template <typename TYPE>
kelvinsSortedList<TYPE>::~kelvinsSortedList(){

    deleteList(listHead);
}

template <typename TYPE>
void kelvinsSortedList<TYPE>::insertItem(TYPE item){

    insertSorted(listHead, item);
}

template <typename TYPE>
void kelvinsSortedList<TYPE>::deleteItem(TYPE item){

    deleteNode(listHead, searchNode(listHead, item));
}

template <typename TYPE>
void kelvinsSortedList<TYPE>::deleteItemAt(int position){

    kelvinsNode<TYPE>* nodeTemp = At(listHead, position);
    if (nodeTemp == NULL){
        return;
    }else {
        deleteNode(listHead, nodeTemp);
    }
}

template <typename TYPE>
void kelvinsSortedList<TYPE>::reverseSortedList(){

    reverse(listHead);
}

template <typename TYPE>
void kelvinsSortedList<TYPE>::printSortedList(){

    printList(listHead);
}

template <typename TYPE>
kelvinsNode<TYPE>* kelvinsSortedList<TYPE>::searchSortedList(TYPE item){

    return search(listHead, item);
}

template <typename TYPE>
int kelvinsSortedList<TYPE>::getSortedListSize(){

    return getCount(listHead);
}

template <typename T>
ostream& operator<<(ostream &out, const kelvinsSortedList<T> &lst){

    kelvinsNode<T>* tempNode = lst.listHead;
    while (tempNode != NULL){
        out << tempNode->getCurrentValue() << "->";
        tempNode = tempNode->getNextAddress();
    }
    return out;
}

template <typename T>
istream& operator>>(istream &in, kelvinsSortedList<T> &lst){

    T tempTYPE;
    in >> tempTYPE;
    lst.insertItem(lst.listHead, tempTYPE);

    return in;
}

#endif // _KELVINS_SORTED_LIST_H_
