#ifndef _KELVINS_LIST_H_
#define _KELVINS_LIST_H_

#include <iostream>
#include "kelvinsNode.h"
#include "kelvinsListHelpers.h"


template <typename TYPE>
class kelvinsList{

private:

    kelvinsNode<TYPE>* kelvinsListHead;

public:

    kelvinsList();
    kelvinsList(kelvinsNode<TYPE>* no);
    ~kelvinsList();

    void insertItem(TYPE item);
    void insertItem(TYPE item, int position);

    void deleteItem(TYPE item);
    void deleteItemAt(int position);

    int getListSize();

    void sortListEx();
    void reverseListEx();
    void randomizeList();

    kelvinsNode<TYPE>* searchListEx(TYPE item);

    void printListEx();

    template <typename T>
    friend ostream& operator<<(ostream &out, const kelvinsList<T> &lst);

    template <typename T>
    friend istream& operator>>(istream &in, kelvinsList<T> &lst);

};

template <typename TYPE>
kelvinsNode<TYPE>* kelvinsList<TYPE>::searchListEx(TYPE item){

    return searchList(item);
}


template <typename TYPE>
kelvinsList<TYPE>::kelvinsList(){

    kelvinsListHead = NULL;
}

template <typename TYPE>
kelvinsList<TYPE>::kelvinsList(kelvinsNode<TYPE>* no){

    kelvinsListHead = no;
}

template <typename TYPE>
kelvinsList<TYPE>::~kelvinsList(){

    deleteList(kelvinsListHead);
}

template <typename TYPE>
int kelvinsList<TYPE>::getListSize(){

    return getCount(kelvinsListHead);
}

template <typename TYPE>
void kelvinsList<TYPE>::insertItem(TYPE item){

    insertHead(kelvinsListHead, item);
}

template <typename TYPE>
void kelvinsList<TYPE>::insertItem(TYPE item, int position){

    if (position < this->getListSize() - 1){
        if (position == 0){
            insertHead(kelvinsListHead, item);
        }else{
            insertAfter(kelvinsListHead, At(kelvinsListHead, position), item);
        }
    }
}

template <typename TYPE>
void kelvinsList<TYPE>::deleteItemAt(int position){

    kelvinsNode<TYPE>* nodeTemp = At(kelvinsListHead, position);
    if (nodeTemp == NULL){
        return;
    }else {
        deleteNode(kelvinsListHead, nodeTemp);
    }
}

template <typename TYPE>
void kelvinsList<TYPE>::deleteItem(TYPE item){

    deleteNode(kelvinsListHead, searchNode(kelvinsListHead, item));
}

template <typename TYPE>
void kelvinsList<TYPE>::sortListEx(){

    sortList(kelvinsListHead);
}

template <typename TYPE>
void kelvinsList<TYPE>::reverseListEx(){

    reverseList(kelvinsListHead);
}

template<typename TYPE>
void kelvinsList<TYPE>::randomizeList(){

    shuffleList(kelvinsListHead);
}

template <typename TYPE>
void kelvinsList<TYPE>::printListEx(){

    printList(kelvinsListHead);
}

template <typename T>
ostream& operator<<(ostream &out, const kelvinsList<T> &lst){

    kelvinsNode<T>* tempNode = lst.kelvinsListHead;
    while (tempNode != NULL){
        out << tempNode->getCurrentValue() << "->";
        tempNode = tempNode->getNextAddress();
    }
    return out;
}

template <typename T>
istream& operator>>(istream &in, kelvinsList<T> &lst){
    T tempTYPE;
    in >> tempTYPE;
    lst.insertItem(lst.kelvinsListHead, tempTYPE);

    return in;
}

#endif //_KELVINS_LIST_H_
