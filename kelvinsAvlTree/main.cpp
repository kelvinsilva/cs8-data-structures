#include <iostream>
#include "kelvinsAvlNode.h"
#include "kelvinsAvlTree.h"

using namespace std;

int main(){

        //insert items
    cout << "Inserting: 9, 5, 10, 0, 6, 11, -1, 1, 2\n\n";
    kelvinsAvlTree<int> myTree;
    myTree.insertItem(9);
    myTree.insertItem(5);
    myTree.insertItem(10);
    myTree.insertItem(0);
    myTree.insertItem(6);
    myTree.insertItem(11);
    myTree.insertItem(-1);
    myTree.insertItem(1);
    myTree.insertItem(2);
    cout << "Tree: \n";
    myTree.printLevelOrder(cout);
    cout << "\n\n";

    cout << "Deleting 10 from tree:\n";
    //delete items
    myTree.deleteItem(10);
    cout << "Tree:\n";

    myTree.printLevelOrder(cout);
    cout << "print preorder: ";
    myTree.printPreOrder(cout);

    return 0;
}

