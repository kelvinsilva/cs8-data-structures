#ifndef _KELVINS_BIN_SEARCH_TREE_H_
#define _KELVINS_BIN_SEARCH_TREE_H_

#include "kelvinsAvlNode.h"
#include <cstring>

//Memory cleanup still needs to be implemented

//Function needed from kelvinsAvlNode
template <typename TYPE>
extern int getNodeHeight(kelvinsAvlNode<TYPE> *node);

template <typename TYPE>
class kelvinsAvlTree{

private:

    kelvinsAvlNode<TYPE>* treeRoot;

    int treeSize;
    int currentLevel;

    void insertItemRecursive(TYPE item, kelvinsAvlNode<TYPE>* &binPtr);
    void deleteItemRecursive(TYPE item, kelvinsAvlNode<TYPE>* &binPtr);
    TYPE deleteMin(kelvinsAvlNode<TYPE>* &binPtr);

    //Obtain height of certain node in tree, for print level order
    int height(kelvinsAvlNode<TYPE>* tempNode);
    void printLevel(ostream& outs, kelvinsAvlNode<TYPE>* tempNode, int level);

    kelvinsAvlNode<TYPE>* searchBinNode (TYPE item, kelvinsAvlNode<TYPE>* binPtr);

    //Rotation functions for AVL
    kelvinsAvlNode<TYPE>* rightRotate(kelvinsAvlNode<TYPE>* nod);
    kelvinsAvlNode<TYPE>* leftRotate(kelvinsAvlNode<TYPE>* nod);

    //rotation rules for AVL
    void rotationRoutine(kelvinsAvlNode<TYPE>* &binPtr, TYPE item);
    void rotationRoutineDelete(kelvinsAvlNode<TYPE>* &binPtr);

    void preOrderRoutine(ostream& outs, kelvinsAvlNode<TYPE>* binPtr);

public:

    kelvinsAvlTree();

    kelvinsAvlNode<TYPE>* searchItem (TYPE item);
    void deleteItem(TYPE item);
    void insertItem(TYPE item);

    void printLevelOrder(ostream& outs);
    void printPreOrder(ostream& outs);

};

//preorder printing
template <typename TYPE>
void kelvinsAvlTree<TYPE>::preOrderRoutine(ostream &outs, kelvinsAvlNode<TYPE> *binPtr){
    if(binPtr != NULL)
    {
        outs << binPtr->getData() << " ";
        preOrderRoutine(outs, binPtr->leftLeaf());
        preOrderRoutine(outs, binPtr->rightLeaf());
    }
}

template <typename TYPE>
void kelvinsAvlTree<TYPE>::printPreOrder(ostream& outs){

    preOrderRoutine(outs, treeRoot);
}

//Pointer manipulation routine to switch a node's leafes to perform a rotation.
//Updates height and manipulates children for proper left rotation
template <typename TYPE>
kelvinsAvlNode<TYPE>* kelvinsAvlTree<TYPE>::leftRotate(kelvinsAvlNode<TYPE>* nod){

    kelvinsAvlNode<TYPE>* n1 = nod->rightLeaf();
    kelvinsAvlNode<TYPE>* n2 = n1->leftLeaf();

    n1->setLeftLeaf(nod);
    nod->setRightLeaf(n2);

    nod->setHeight( maxOf( getNodeHeight(nod->leftLeaf()), getNodeHeight(nod->rightLeaf())) + 1);
    n1->setHeight( maxOf( getNodeHeight(n1->leftLeaf()), getNodeHeight(n1->rightLeaf())) + 1);

}

//Pointer manipulation routine to switch a node's leafes to perform a rotation.
//Updates height and manipulates children for proper right rotation
template <typename TYPE>
kelvinsAvlNode<TYPE>* kelvinsAvlTree<TYPE>::rightRotate(kelvinsAvlNode<TYPE>* nod){

    kelvinsAvlNode<TYPE> *n1 = nod->leftLeaf();
    kelvinsAvlNode<TYPE> *n2 = n1->rightLeaf();

    n1->setRightLeaf(nod);
    nod->setLeftLeaf(n2);

    nod->setHeight( maxOf( getNodeHeight(nod->leftLeaf()), getNodeHeight(nod->rightLeaf())) + 1);
    n1->setHeight( maxOf( getNodeHeight(n1->leftLeaf()), getNodeHeight(n1->rightLeaf())) + 1);

    return n1;
}

template <typename TYPE>
kelvinsAvlTree<TYPE>::kelvinsAvlTree(){

    treeSize = 0;
    treeRoot = NULL;
}

template <typename TYPE>
void kelvinsAvlTree<TYPE>::insertItemRecursive(TYPE item, kelvinsAvlNode<TYPE>* &binPtr){

    //perform normal insertion, but after each insertion, perform a check for rotation cases
    if (treeSize == 0){

        binPtr = new kelvinsAvlNode<TYPE>(item);
    }else {

        if (item < binPtr->getData()){

            if (binPtr->leftLeaf() == NULL){

                binPtr->setLeftLeaf( new kelvinsAvlNode<TYPE>(item) );
                rotationRoutine(binPtr, item);  //insert and check for rotation case
                return;
            }
            insertItemRecursive(item, binPtr->leftLeaf());
            rotationRoutine(binPtr, item);
        }else if (item >= binPtr->getData()) {

            if (binPtr->rightLeaf() == NULL){

                binPtr->setRightLeaf( new kelvinsAvlNode<TYPE>(item) );  //insert and check for rotation case
                rotationRoutine(binPtr, item);
                return;
            }

            insertItemRecursive(item, binPtr->rightLeaf());  //insert and check for rotation case
            rotationRoutine(binPtr, item);
        }
    }

}

//Handles the four cases in which to rotate a binary tree node. Used for insertion.
template <typename TYPE>
void kelvinsAvlTree<TYPE>::rotationRoutine(kelvinsAvlNode<TYPE>* &binPtr, TYPE item){

    //update height and obtain balance factor
    binPtr->setHeight(maxOf(getNodeHeight(binPtr->leftLeaf()), getNodeHeight(binPtr->rightLeaf())) + 1);
    int balance = binPtr->getBalanceFactor();

    //left left case
    if (balance > 1 && (item < binPtr->leftLeaf()->getData())){

        binPtr = rightRotate(binPtr);
    }

    //right right case
    if (balance < -1 && (item > binPtr->rightLeaf()->getData()) ){

        binPtr = leftRotate(binPtr);
    }

    //left right case
    if (balance > 1 && (item > binPtr->leftLeaf()->getData())){

        binPtr->setLeftLeaf(leftRotate(binPtr->leftLeaf()));
        binPtr = rightRotate(binPtr);
    }

    //right left case
    if (balance < -1 && (item < binPtr->rightLeaf()->getData())){

        binPtr->setRightLeaf(rightRotate(binPtr->rightLeaf()));
        binPtr = leftRotate(binPtr);
    }
}

template <typename TYPE>
void kelvinsAvlTree<TYPE>::insertItem(TYPE item){

    insertItemRecursive(item, treeRoot);
    ++treeSize;
}

template <typename TYPE>
void kelvinsAvlTree<TYPE>::deleteItem(TYPE item){

    deleteItemRecursive(item, treeRoot);
    --treeSize;
}

//Handles a four cases in which to rotate binary tree nodes for deletion
template <typename TYPE>
void kelvinsAvlTree<TYPE>::rotationRoutineDelete(kelvinsAvlNode<TYPE>* &binPtr){

    binPtr->setHeight( maxOf(getNodeHeight(binPtr->leftLeaf()), getNodeHeight(binPtr->rightLeaf())) + 1);

    int balance = binPtr->getBalanceFactor();

    //if balance factor is greater than 1 that means the node is out of balance
    //check if left child is out of balance to the right. if it is, then
    //--left left case, means need right rotation
    if (balance > 1 && binPtr->leftLeaf()->getBalanceFactor() >= 0){

        binPtr = rightRotate(binPtr);
    }

    //if balance factor is greater tahn 1, that means the node is out of balance
    //if the left leaf is less than 0 that means it is unbalanced to the right
    //--left right case, first rotate left and turn into left left, then rotate right to fix left left
    if (balance > 1 && binPtr->leftLeaf()->getBalanceFactor() < 0){

        binPtr->setLeftLeaf( leftRotate(binPtr->leftLeaf()) );
        binPtr = rightRotate(binPtr);
    }

    //Same thing as above but right right case means need left rotation to fix
    if (balance < -1 && binPtr->rightLeaf()->getBalanceFactor() <= 0){

        binPtr = leftRotate(binPtr);
    }

    //same as above, but right left case needs, right rotation, then left rotation to fix node
    if (balance < -1 && binPtr->rightLeaf()->getBalanceFactor() > 0){

        binPtr->setRightLeaf( rightRotate(binPtr->rightLeaf()) );
        binPtr = leftRotate(binPtr);
    }

}

template <typename TYPE>
void kelvinsAvlTree<TYPE>::deleteItemRecursive(TYPE item, kelvinsAvlNode<TYPE>* &binPtr){

    //perform normal binary search tree deletion, except at the end of each recursive call, we need to check for rotation cases.
    if (binPtr != NULL){

        if ( item < binPtr->getData()){

            //perform deletion
            deleteItemRecursive(item, binPtr->leftLeaf());
            //check for rotation cases
            rotationRoutineDelete(binPtr);
        }else if (item > binPtr->getData()){

            //perform deletion
            deleteItemRecursive(item, binPtr->rightLeaf());
            //check for rotation cases
            rotationRoutineDelete(binPtr);
        }else if (binPtr->isLeaf()){    //leftchild == NULL and rightChil == NULL

            kelvinsAvlNode<TYPE>* tempPrevNode = treeRoot;

            //This while loop searches for the node before the node to become deleted
            while (tempPrevNode->leftLeaf() != binPtr && tempPrevNode->rightLeaf() != binPtr){

                if (binPtr->getData() < tempPrevNode->getData()){

                    tempPrevNode = tempPrevNode->leftLeaf();
                }else if (binPtr->getData() > tempPrevNode->getData()){

                    tempPrevNode = tempPrevNode->rightLeaf();
                }
            }
            if (tempPrevNode->leftLeaf() == binPtr){

                tempPrevNode->leftLeaf() = NULL;
            }else if (tempPrevNode->rightLeaf() == binPtr){

                tempPrevNode->rightLeaf() = NULL;
            }

            delete binPtr;
            binPtr = NULL;
            return;
        }else if (binPtr->leftLeaf() == NULL){
            //when we reach a null leaf, that means there is a right leaf that is populated and we need to move it to the parent.
            kelvinsAvlNode<TYPE>* temp = binPtr->rightLeaf();
            kelvinsAvlNode<TYPE>* tempAddr = binPtr;
            delete binPtr;

            memmove(tempAddr, temp, sizeof(kelvinsAvlNode<TYPE>));
            return;

        }else if (binPtr->rightLeaf() == NULL){
            //same as above
            kelvinsAvlNode<TYPE>* temp = binPtr->leftLeaf();
            kelvinsAvlNode<TYPE>* tempAddr = binPtr;

            delete binPtr;
            memmove(tempAddr, temp, sizeof(kelvinsAvlNode<TYPE>));
            return;

        }else {

            binPtr->setData( deleteMin(binPtr->rightLeaf()) );
        }
    }
}

//Delete the lowest valued element of descedants of right child.
//Data structures and algorithms by alfred v aho, john e hopcroft, jeffrey d ullman
template <typename TYPE>
TYPE kelvinsAvlTree<TYPE>::deleteMin(kelvinsAvlNode<TYPE>* &binPtr){

    if (binPtr->leftLeaf() == NULL){

        TYPE item = binPtr->getData();
        binPtr = binPtr->rightLeaf();

        return item;
    } else {

        deleteMin(binPtr->leftLeaf());
    }
}

//performs a search and returns address of node.
template <typename TYPE>
kelvinsAvlNode<TYPE>* kelvinsAvlTree<TYPE>::searchBinNode (TYPE item, kelvinsAvlNode<TYPE>* binPtr){

    if (item < binPtr->getData()){

        return searchBinNode(item, binPtr->leftLeaf());
    }else if (item > binPtr->getData()){

        return searchBinNode(item, binPtr->rightLeaf());
    }
    if (binPtr->getData() == item){
        return binPtr;
    }
}

template <typename TYPE>
kelvinsAvlNode<TYPE>* kelvinsAvlTree<TYPE>::searchItem (TYPE item){

    return searchBinNode(item, treeRoot);
}

//finds total height of tree for print level order
template <typename TYPE>
int kelvinsAvlTree<TYPE>::height(kelvinsAvlNode<TYPE>* tempNode){

    if (tempNode == NULL){

        return 0;
    }else {

        int leftHeight = height(tempNode->leftLeaf());
        int rightHeight = height(tempNode->rightLeaf());

        if (leftHeight > rightHeight){

            return (leftHeight + 1);
        }else {

            return (rightHeight + 1);
        }
    }
}

//print each individual level from print level order
template <typename TYPE>
void kelvinsAvlTree<TYPE>::printLevel(ostream& outs, kelvinsAvlNode<TYPE>* tempNode, int level){

    //put a star if node is unfilled
    if (tempNode == NULL){
        cout << " * ";
        return;
    }
    if (level == 1){

        outs << tempNode->getData() << " ";
    }else if (level > 1){

        printLevel(cout, tempNode->leftLeaf(), level-1);
        printLevel(cout, tempNode->rightLeaf(), level-1);
    }
}

//print each level and a new line/
template <typename TYPE>
void kelvinsAvlTree<TYPE>::printLevelOrder(ostream& outs){

    int h = height(treeRoot);
    for(int i = 1; i <= h; i++){

        printLevel(outs, treeRoot, i);
        cout << "\n";
    }
}

#endif // _KELVINS_BIN_SEARCH_TREE_H_
