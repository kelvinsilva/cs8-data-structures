#ifndef _KELVINS_AVL_NODE_H_
#define _KELVINS_AVL_NODE_H_

#include <iostream>


template <typename TYPE>
class kelvinsAvlNode;

template <typename T>
int getNodeHeight(kelvinsAvlNode<T> *node);

//Helper function max:
int maxOf(int x, int y){

    if (x >= y){
        return x;
    }else if (x < y){
        return y;
    }
}

using namespace std;

template <typename TYPE>
class kelvinsAvlNode{

private:

    TYPE currentValue;
    //int balanceFactor;
    int height;
    kelvinsAvlNode<TYPE>* leftLeafNode;
    kelvinsAvlNode<TYPE>* rightLeafNode;

public:

    kelvinsAvlNode();
    kelvinsAvlNode(TYPE it);
    //modification member funx
    TYPE& getData();
    kelvinsAvlNode<TYPE>* &leftLeaf();
    kelvinsAvlNode<TYPE>* &rightLeaf();
    void setData(const TYPE& newData);
    void setLeftLeaf(kelvinsAvlNode<TYPE>* newLeaf);
    void setRightLeaf(kelvinsAvlNode<TYPE>* newLeaf);

    //constant
    const TYPE& getData() const;
    const kelvinsAvlNode<TYPE>* leftLeaf() const;
    const kelvinsAvlNode<TYPE>* rightLeaf() const;
    bool isLeaf() const;

    //balance factor modification
    //void setBalanceFactor(int bf);
    int getBalanceFactor();

    //getters and setters
    int getHeight();
    void setHeight(int h);

    template <typename T>
    friend int getNodeHeight(kelvinsAvlNode<T>* node);

};


template <typename T>
int getNodeHeight(kelvinsAvlNode<T>* node){

    if (node == NULL){
        return 0;
    }else {
        return node->getHeight();
    }
}

template <typename TYPE>
int kelvinsAvlNode<TYPE>::getHeight(){

    return  this->height;
}

template <typename TYPE>
void kelvinsAvlNode<TYPE>::setHeight(int h){

    height = h;
}

template <typename TYPE>
kelvinsAvlNode<TYPE>::kelvinsAvlNode(){

    leftLeafNode = NULL;
    rightLeafNode = NULL;
    //balanceFactor = 0;
    height = 1;
}

template <typename TYPE>
kelvinsAvlNode<TYPE>::kelvinsAvlNode(TYPE it){

    leftLeafNode = NULL;
    rightLeafNode = NULL;
    currentValue = it;
    //balanceFactor = 0;
    height = 1;
}

template <typename TYPE>
TYPE& kelvinsAvlNode<TYPE>::getData(){

    return currentValue;
}

template <typename TYPE>
kelvinsAvlNode<TYPE>*& kelvinsAvlNode<TYPE>::leftLeaf(){

    return leftLeafNode;
}

template <typename TYPE>
kelvinsAvlNode<TYPE>*& kelvinsAvlNode<TYPE>::rightLeaf(){

    return rightLeafNode;
}

template <typename TYPE>
void kelvinsAvlNode<TYPE>::setData(const TYPE& newData){

    currentValue = newData;
}

template <typename TYPE>
void kelvinsAvlNode<TYPE>::setLeftLeaf(kelvinsAvlNode<TYPE>* newLeaf){

    leftLeafNode = newLeaf;
}

template <typename TYPE>
void kelvinsAvlNode<TYPE>::setRightLeaf(kelvinsAvlNode<TYPE>* newLeaf){

    rightLeafNode = newLeaf;
}

template <typename TYPE>
const TYPE& kelvinsAvlNode<TYPE>::getData() const{

    return currentValue;
}

template <typename TYPE>
const kelvinsAvlNode<TYPE>* kelvinsAvlNode<TYPE>::leftLeaf() const{

    return leftLeafNode;
}

template <typename TYPE>
const kelvinsAvlNode<TYPE>* kelvinsAvlNode<TYPE>::rightLeaf() const{

    return rightLeafNode;
}

template <typename TYPE>
bool kelvinsAvlNode<TYPE>::isLeaf() const{

    return ((rightLeafNode == NULL) && (leftLeafNode == NULL));
}

/*
template <typename TYPE>
void kelvinsAvlNode<TYPE>::setBalanceFactor(int bf){

    balanceFactor = bf;
}*/

//balance factor of a tree, is the height of left side minnus height of right side.
template <typename TYPE>
int kelvinsAvlNode<TYPE>::getBalanceFactor(){

    if (leftLeafNode == NULL && rightLeafNode == NULL){
        return 0;
    }
    return (getNodeHeight(leftLeafNode) - getNodeHeight(rightLeafNode));
}


#endif // _KELVINS_AVL_NODE_H_
