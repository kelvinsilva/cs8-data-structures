#include <iostream>
#include <list>


using namespace std;

void kelvinsInsertionSort(list<int>* sortThis);

int main(){

  list<int> myList;
  myList.push_back(27);
  myList.push_back(47777);
  myList.push_back(21);
  myList.push_back(12);
  myList.push_back(19);  
  myList.push_back(28);
  myList.push_back(-12);
  myList.push_back(17);  
  myList.push_back(28);
  myList.push_back(12);
  myList.push_back(11);
  myList.push_back(577);
  
  kelvinsInsertionSort(&myList);
  
  for (list<int>::iterator it = myList.begin(); it != myList.end(); ++it){
    cout << *it << " ";
  }
  
  cout << endl;
  return 0;
}

void kelvinsInsertionSort(list<int>* sortThis){
  
  list<int>::iterator it = sortThis->begin();
  list<int>::iterator temp_it = sortThis->begin();
  list<int>::iterator prev_it = sortThis->begin();
  int listSize = sortThis->size();
  int sortedSize = 0;
  
  for ( ;it != sortThis->end(); ++it){
    
    if (sortedSize > 0){
      prev_it = --it;
      ++it;
      if (*it < *prev_it ){
        temp_it = sortThis->begin();
        while (*temp_it < *it){
          
          ++temp_it;
        }

        sortThis->insert(temp_it, *it);
        it = sortThis->erase(it);
        --it;
      }
    }
    ++sortedSize;
  }

}



