#include <iostream>
#include "graph.h"


using namespace std;

//page 792
template <class Item, class SizeType>
void rec_dfs(graph<Item>& g, SizeType v, bool marked[]){
    
    set<size_t> connections = g.neighbors(v);
    set<size_t>::iterator it;
    
    marked[v] = true;
    for (it = connections.begin(); it != connections.end(); ++it){
        
        if (!marked[*it]){
            
            rec_dfs(g, *it, marked);
        }
    }
}

template <typename Item, typename SizeType >
bool directedPath(graph<Item>& g, SizeType start, int end){
    
    /*From pp 794 in savitch: "Either breadth first search or depth first search may be used to determine whether a  path exists between u and v"*/
    
    //depth first method
    bool marked[g.MAXIMUM];
    
    for (int i = 0; i < g.MAXIMUM; i++){
        marked[i] = false;
    }
    assert(start < g.size());
    fill_n(marked, g.size(), false);
    rec_dfs(g, start, marked);
    
    if (marked[end] == true){
        
        return true;
    }else {
        
        return false;
    }
    
    
}

string convertBoolToString(bool b){
    
    if (b) {
        
        return "True";
    }else {
        
        return "False";
    }
}

int main(){
    
    //Testing function without class.
    graph<int> mygraph(50);
        
    mygraph.add_vertex(1);
    mygraph.add_vertex(4);
    mygraph.add_edge(0, 1);
    mygraph.add_vertex(5);
    mygraph.add_edge(1, 2);
    
    //0 -> 1 -> 2
    // 0 -/> 9
    cout << endl << "Path from 0, 1 " << convertBoolToString(directedPath(mygraph, 0, 1) );
    cout << endl << "Path from 1, 2 " << convertBoolToString(directedPath(mygraph, 1, 2) );
    cout << endl << "Path from 0, 2 " << convertBoolToString(directedPath(mygraph, 0, 2) );
    cout << endl << "Path from 0, 9 " << convertBoolToString(directedPath(mygraph, 0, 9) );
    
    
    return 0;
}

