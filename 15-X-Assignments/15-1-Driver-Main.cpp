#include <iostream>
#include "graph.h"

using namespace std;

int main() {
    
    graph<int> mygraph(50);
    
    mygraph.add_vertex(1);
    mygraph.add_vertex(4);
    mygraph.add_edge(0, 1);
    
    mygraph.resize(80);

    return 0;
}