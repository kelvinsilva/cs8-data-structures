#ifndef _GRAPH_
#define _GRAPH_

//kelvin da silva

#include <cstdlib>
#include <set>
#include <cassert>

using namespace std;

template <class Item>
class graph{
    
private:

    bool **edges; //2d array edges
    Item *labels;  //labels array
    size_t allocated;
    size_t many_vertices;
    
    bool directedPath(SizeType start, int end);
    void rec_dfs(SizeType v, bool marked[]);
    
public:    

    std::size_t MAXIMUM;
    graph(int n);
    ~graph();
    
    void add_vertex(const Item& label);
    void add_edge(size_t source, size_t target);
    void remove_edge(size_t source, size_t target);
    
    Item& operator[](size_t vertex);
    
    size_t size() const {return many_vertices; }
    void resize(int n);
    
    bool is_edge(size_t source, size_t target) const;
    set<size_t> neighbors(size_t vertex) const;
    Item& operator[] (size_t vertex) const;
    
    bool hasDirectedPath(int start, int end);

};

template <class Item>
void graph<Item>::resize(int n){
    
    //create new space
    bool ** tempEdges;
    Item *tempLabels;
    
    tempEdges = new bool*[n];
    for (int i = 0; i < n; i++){
        
        tempEdges[i] = new bool[n];
    }
    
    tempLabels = new Item[n];
    MAXIMUM = n;
    
     //transfer data from current into new space
    for (int i = 0 ; i < allocated; ++i){
        for (int j = 0; j < allocated; ++j){
            
            tempEdges[i][j] = edges[i][j];
        }
    }
    
    for (int i = 0; i < allocated; ++i){
        tempLabels[i] = labels[i];
    }
    
    //delete current space
    for (int i = 0; i < allocated; i++){
        
        delete [] edges[i];
    }
    delete [] edges;
    delete [] labels;
    
    //assign class member pointer to pointer of new space
    edges = tempEdges;
    labels = tempLabels;
    //update private member
    allocated = n;
}

template <class Item>
graph<Item>::graph(int n){ 
    
    many_vertices = 0; 
    allocated = n;
    
    edges = new bool*[n];
    for (int i = 0; i < n; i++){
        
        edges[i] = new bool[n];
    }
    
    labels = new Item[n];
    MAXIMUM = n;
  
}

template <class Item>
graph<Item>::~graph<Item>(){

    for (int i = 0; i < allocated; i++){
        
        delete [] edges[i];
    }
    delete [] edges;
    delete [] labels;
    
}

bool hasDirectedPath(int start, int end){
    
    return directedPath(start, end);
}

//page 792 walter savitch
template <class Item, class SizeType>
void rec_dfs(SizeType v, bool marked[]){
    
    set<size_t> connections = this->neighbors(v);
    set<size_t>::iterator it;
    
    marked[v] = true;
    for (it = connections.begin(); it != connections.end(); ++it){
        
        if (!marked[*it]){
            
            rec_dfs(*it, marked);
        }
    }
}

template <typename Item, typename SizeType >
bool directedPath(SizeType start, int end){
    
    /*From pp 794 in savitch: "Either breadth first search or depth first search may be used to determine whether a  path exists between u and v"*/
    
    //depth first method
    bool marked[this->MAXIMUM];
    
    for (int i = 0; i < this->MAXIMUM; i++){
        marked[i] = false;
    }
    assert(start < this->size());
    fill_n(marked, this->size(), false);
    rec_dfs(start, marked);
    
    if (marked[end] == true){
        
        return true;
    }else {
        
        return false;
    }
    
    
}
