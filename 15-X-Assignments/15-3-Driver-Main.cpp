#include <iostream>
#include "graph.h"
#include "inheritedGraph-15-3.h"

using namespace std;

int main() {
    
    graph<int> mygraph(50);
    
    mygraph.add_vertex(1);
    mygraph.add_vertex(4);
    mygraph.add_edge(0, 1);
    
    mygraph.resize(80);
    
    inheritedGraph<int, string> myInhGraph(50);
    myInhGraph.add_vertex(1);
    myInhGraph.add_vertex(4);
    myInhGraph.add_vertex(4);
    myInhGraph.add_vertex(4);
    
    myInhGraph.add_edge_label(0, 1, "edge from 0 to 1");
    myInhGraph.add_edge_label(0, 2, "edge from 0 to 2");
    myInhGraph.add_edge_label(0, 3, "edge from 0 to 3");
    
    
    //should output "first edge"
    cout << endl << myInhGraph.get_edge_label(0, 1);
    cout << endl << myInhGraph.get_edge_label(0, 2);    
    cout << endl << myInhGraph.get_edge_label(0, 3);    
    
    return 0;
}