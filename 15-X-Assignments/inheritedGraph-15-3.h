#include "graph.h"

using namespace std;

template <class vertexLabel, class T>
class inheritedGraph : public graph<vertexLabel>{
    
private:

T **edgeLabels;

public:
    
    inheritedGraph(int n) : graph<vertexLabel>(n){
        
        edgeLabels = new T*[n];
        for (int i = 0; i < n; i++){
            
            edgeLabels[i] = new T[n];
        }
    }
    
    void add_edge_label(size_t source, size_t target, T thing);
    T get_edge_label(size_t source, size_t target);
    
};

template <class vertexLabel, class T>
void inheritedGraph<vertexLabel, T>::add_edge_label(size_t source, size_t target, T thing){
    
    assert(source < this->size() );
    assert(target < this->size() );
    
    this->add_edge(source, target);
    edgeLabels[source][target] = thing;
}

template <class vertexLabel, class T>
T inheritedGraph<vertexLabel, T>::get_edge_label(size_t source, size_t target){
    
    assert( this->is_edge(source, target) == true);
    return edgeLabels[source][target];
}