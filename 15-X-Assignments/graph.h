#ifndef _GRAPH_
#define _GRAPH_

//kelvin da silva

#include <cstdlib>
#include <set>
#include <cassert>

using namespace std;

template <class Item>
class graph{
    
private:

    bool **edges; //2d array edges
    Item *labels;  //labels array
    size_t allocated;
    size_t many_vertices;
    
public:    

    std::size_t MAXIMUM;
    graph(int n);
    ~graph();
    
    void add_vertex(const Item& label);
    void add_edge(size_t source, size_t target);
    void remove_edge(size_t source, size_t target);
    
    Item& operator[](size_t vertex);
    
    size_t size() const {return many_vertices; }
    void resize(int n);
    
    bool is_edge(size_t source, size_t target) const;
    set<size_t> neighbors(size_t vertex) const;
    Item& operator[] (size_t vertex) const;

};

template <class Item>
void graph<Item>::resize(int n){
    
    //create new space
    bool ** tempEdges;
    Item *tempLabels;
    
    tempEdges = new bool*[n];
    for (int i = 0; i < n; i++){
        
        tempEdges[i] = new bool[n];
    }
    
    tempLabels = new Item[n];
    MAXIMUM = n;
    
     //transfer data from current into new space
    for (int i = 0 ; i < allocated; ++i){
        for (int j = 0; j < allocated; ++j){
            
            tempEdges[i][j] = edges[i][j];
        }
    }
    
    for (int i = 0; i < allocated; ++i){
        tempLabels[i] = labels[i];
    }
    
    //delete current space
    for (int i = 0; i < allocated; i++){
        
        delete [] edges[i];
    }
    delete [] edges;
    delete [] labels;
    
    //assign class member pointer to pointer of new space
    edges = tempEdges;
    labels = tempLabels;
    //update private member
    allocated = n;
}

template <class Item>
graph<Item>::graph(int n){ 
    
    many_vertices = 0; 
    allocated = n;
    
    edges = new bool*[n];
    for (int i = 0; i < n; i++){
        
        edges[i] = new bool[n];
    }
    
    labels = new Item[n];
    MAXIMUM = n;
  
}

template <class Item>
graph<Item>::~graph<Item>(){

    for (int i = 0; i < allocated; i++){
        
        delete [] edges[i];
    }
    delete [] edges;
    delete [] labels;
    
}


template <class Item>
void graph<Item>::add_edge(size_t source, size_t target){
    
    assert(source < size() );
    assert(target < size() );
    edges[source][target] = true;
}

template <class Item>
void graph<Item>::add_vertex(const Item& label){
    
    size_t new_vertex_number;
    size_t other_number;
    
    assert(size() < MAXIMUM);
    new_vertex_number = many_vertices;
    ++many_vertices;
    for (other_number = 0; other_number < many_vertices; ++other_number){

        edges[other_number][new_vertex_number] = false;
        edges[new_vertex_number][other_number] = false;
    }
    labels[new_vertex_number] = label;
}

template <class Item>
bool graph<Item>::is_edge(size_t source, size_t target) const{
    
    assert(source < size() );
    assert(target < size() );
    return edges[source][target];
}

template <class Item>
Item& graph<Item>::operator[] (size_t vertex){
    
    assert(vertex < size() );
    return labels[vertex];
}

template <class Item>
Item& graph<Item>::operator[] (size_t vertex) const {
    
    assert(vertex < size() );
    return labels[vertex];
}

template <class Item>
set<size_t> graph<Item>::neighbors(size_t vertex) const {
    
    set<size_t> answer;
    size_t i;
    
    assert(vertex < size() );
    
    for (i = 0; i < size(); ++i){
        
        if (edges[vertex][i]){
            answer.insert(i);
        }
    }
    return answer;
}

template <class Item>
void graph<Item>::remove_edge(size_t source, size_t target){
    
    assert(source < size() );
    assert(target < size() );
    edges[source][target] = false;
    
}

#endif